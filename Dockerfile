# Test stage
FROM node:16 AS tester
WORKDIR /app
COPY . .
ENV CI=true
RUN npm install && npm run test

# Build stage
FROM tester as builder
WORKDIR /app
ENV CI=true
RUN npm run build

# Build light image with nginx and static files from Build stage
FROM 074919163324.dkr.ecr.us-east-1.amazonaws.com/nginx-core-spa:poc AS runner
COPY --from=builder /app/build /app