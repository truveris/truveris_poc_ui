.SILENT: clear

version:
	@git describe --tags --always | sed 's/-/./g'

build: login build-server

build-server:
	docker build -t ${ECR_SERVER}:${CONTAINER_ID} .

run-server:
	APP_VERSION=${VERSION} docker run --rm ${ECR_SERVER}:${CONTAINER_ID}

tests:
	docker build --target tester .

login:
	@echo "Login into ECR"
	`aws ecr get-login --no-include-email --region ${AWS_REGION}`

push: push-server

push-server:
	docker push ${ECR_SERVER}:${CONTAINER_ID}

run.local: build-client
	docker-compose -f docker-compose.dev.yml up

run.remote: build-client
	docker-compose -f docker-compose.prod.yml up

build-client:
	docker-compose -f docker-compose.prod.yml build client
