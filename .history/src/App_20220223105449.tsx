import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./store/hooks";
import { RfpActionTypes } from "./domain/rfp/RfpActionTypes";

export const App = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchRfpsActionCreator);
  }, [dispatch]);

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
    </div>
  );
};
