import { configureStore, ThunkAction, Action, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { rfpReducer } from '../domain/rfp/RfpReducer';
import { watchRfpsAsyncActions } from '../domain/rfp/RfpSagas';

let sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    rfpReducer: rfpReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
    thunk: false
  }).concat(sagaMiddleware)
});

sagaMiddleware.run(watchRfpsAsyncActions);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
