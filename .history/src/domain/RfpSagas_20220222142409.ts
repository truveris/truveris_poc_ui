import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import {
   UpdateReportsAction,
   storeReportsActionCreator,
   updateReportsReceivedActionCreator,
   storeTaskExecutionResponseActionCreator
} from './ReportsActionCreators';
import {saga} from '@unifocus/react-application';
import {ReportsDataModel, UpdateExecutionModel} from './ReportsModel';
import {getReportsAdapter, updateReportsAdapter} from '../../adapters/reportsAdapter/ReportsAdapter';
import {ReportsActionTypes} from './ReportsActionTypes';

export function* getReportsSaga(action: UpdateReportsAction): SagaIterator {
   yield put(updateReportsReceivedActionCreator(false));

   const urlReports = "/reporter/api/reports";

   const reports: ReportsDataModel = yield call(getReportsAdapter, urlReports);

   yield put(storeReportsActionCreator(reports));
}

export function* watchReportsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(ReportsActionTypes.GET_REPORTS,
                           saga(getReportsSaga, { disableMasking: true })),
                takeLatest(ReportsActionTypes.UPDATE_REPORTS,
                           saga(updateReportsExecutionSaga, { disableMasking: true }))
             ]);
}