import { createAction } from "@reduxjs/toolkit";
import { RfpActionTypes } from "./RfpActionTypes";
import { RfpModel } from "./RfpModels";

// export interface StoreRfpsAction {
//     type: RfpActionTypes.STORE_RFPS;
//     rfps: RfpModel[];
//  }
 
//  export const storeRfpsActionCreator = (rfps: RfpModel[]): StoreRfpsAction => {
//     return {
//        type: RfpActionTypes.STORE_RFPS,
//        rfps: rfps,
//     };
//  };

const storeRfpsActionCreator = createAction<RfpModel[]>(RfpActionTypes.STORE_RFPS)

const addTodo = createAction('todos/add', function prepare(text: string) {
    return {
      payload: {
        text,
        id: nanoid(),
        createdAt: new Date().toISOString(),
      },
    }
  })