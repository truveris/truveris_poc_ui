import { LocalDate } from "@js-joda/core";
import { PropSchema, serializable } from "serializr";
import { localDate } from "../../utilities/serialization/MobxTypes";
export class RfpModel {
    @serializable
    id: number;

    @serializable
    awarded_at: string;
 
    @serializable
    client_name: string;

    @serializable
    contract_pbm: string;

    @serializable(localDate())
    implementation_date: LocalDate;

    @serializable(localDate())
    next_milestone: LocalDate;

    @serializable(localDate())
    rfp_sent: LocalDate;

    @serializable
    notes: string;

    @serializable
    rfp_status: string;

    @serializable
    year: string;
 }