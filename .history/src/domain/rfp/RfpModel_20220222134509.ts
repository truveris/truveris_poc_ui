import { serializable } from "serializr";

export class TagsModel {
    @serializable
    id: string;
 
    @serializable
    label: string;
 }