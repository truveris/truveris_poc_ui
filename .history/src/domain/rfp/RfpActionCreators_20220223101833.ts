
export interface StoreReportsAction {
    type: ReportsActionTypes.STORE_REPORTS;
    reports: ReportsModel[];
    selfLink: LinkModel;
 }
 
 export const storeReportsActionCreator = (reports: ReportsDataModel): StoreReportsAction => {
    return {
       type: ReportsActionTypes.STORE_REPORTS,
       reports: reports.reports,
       selfLink: reports.selfLink
    };
 };
 