import { createAction } from "@reduxjs/toolkit";
import { RfpActionTypes } from "./RfpActionTypes";
import { RfpModel } from "./RfpModels";

// export interface StoreRfpsAction {
//     type: RfpActionTypes.STORE_RFPS;
//     rfps: RfpModel[];
//  }
 
//  export const storeRfpsActionCreator = (rfps: RfpModel[]): StoreRfpsAction => {
//     return {
//        type: RfpActionTypes.STORE_RFPS,
//        rfps: rfps,
//     };
//  };

const storeRfpsActionCreator = createAction(RfpActionTypes.STORE_RFPS, (rfps: RfpModel[]) => {} {
    return {
      payload: {
        rfps,
      },
    }
  })