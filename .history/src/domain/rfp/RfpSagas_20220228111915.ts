import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import { fetchRfpsAdapter } from '../../adapters/RfpAdapter';
import { storeRfpsActionCreator } from './RfpActionCreators';
import { RfpActionTypes } from './RfpActionTypes';
import { RfpModel } from './RfpModels';

export function* getRfpsSaga(): SagaIterator {

   // TODO MP - replace with real url
   const urlRfps = "https://run.mocky.io/v3/4315596e-deef-4f77-996e-5a147fd6029f";
   const jwt = localStorage.getItem('myCat')
   console.log('JWT:' + const cat = localStorage.getItem('myCat'))

   const rfps: RfpModel[] = yield call(fetchRfpsAdapter, urlRfps);

   yield put(storeRfpsActionCreator(rfps));
}

export function* watchRfpsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(RfpActionTypes.FETCH_RFPS, getRfpsSaga),
             ]);
}