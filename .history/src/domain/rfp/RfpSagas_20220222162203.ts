import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import { fetchRfpsAdapter } from '../../adapters/RfpAdapter';
import { RfpActionTypes } from './RfpActionTypes';
import { RfpModel } from './RfpModels';
import { storeRfpsAction } from './RfpReducer';

export function* getRfpsSaga(): SagaIterator {
   const urlRfps = "https://run.mocky.io/v3/34a24a00-428c-4ab6-8d0d-a06f8ad76399";

   const rfps: RfpModel[] = yield call(fetchRfpsAdapter, urlRfps);

   yield put(storeRfpsAction(rfps));
}

export function* watchRfpsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(RfpActionTypes.FETCH_RFPS, getRfpsSaga),
             ]);
}