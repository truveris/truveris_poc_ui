import { createSlice } from '@reduxjs/toolkit';
import { RfpModel } from './RfpModels';

export interface ReportsReducerState {
    rfps: RfpModel[];
}

const initialState: ReportsReducerState = {
  rfps: [],
};

const counterReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(increment, (state, action) => {
      state.value++
    })
    .addCase(decrement, (state, action) => {
      state.value--
    })
    .addCase(incrementByAmount, (state, action) => {
      state.value += action.payload
    })
})

// export const rfpsSlice = createSlice({
//   name: 'rfpSlice',
//   initialState,
//   reducers: {
//     storeRfpsAction: (state, action) => {
//         return {
//            rfps: action.payload
//         };
//       }
//   }
// });

// export const { storeRfpsAction } = rfpsSlice.actions;

// export default rfpsSlice.reducer;