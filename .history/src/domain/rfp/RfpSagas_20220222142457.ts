import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import {
   UpdateReportsAction,
   storeReportsActionCreator,
   updateReportsReceivedActionCreator,
   storeTaskExecutionResponseActionCreator
} from './ReportsActionCreators';


export function* getReportsSaga(action: UpdateReportsAction): SagaIterator {
   yield put(updateReportsReceivedActionCreator(false));

   const urlReports = "/reporter/api/reports";

   const reports: ReportsDataModel = yield call(getReportsAdapter, urlReports);

   yield put(storeReportsActionCreator(reports));
}

export function* watchReportsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(ReportsActionTypes.GET_REPORTS,
                           saga(getReportsSaga)),
             ]);
}