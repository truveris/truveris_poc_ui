import { serializable } from "serializr";

export class TagsModel {
    @serializable
    id: string;
 
    @serializable
    label: string;
 
    @serializable
    @optional
    parentId: string;
 
    @serializable
    @optional
    abbreviation: string;
 }