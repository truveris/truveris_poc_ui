import { serializable } from "serializr";

export class TagsModel {
    @serializable
    id: number;

    @serializable
    awarded_at: string;
 
    @serializable
    client_name: string;

    @serializable
    contract_pbm: string;

    @serializable
    implementation_date: Date;

    @serializable
    next_milestone: Date;

    @serializable
    rfp_sent: Date;

    @serializable
    notes: string;
    
    @serializable
    rfp_status: string;

    rfp_status

 }