import { RfpActionTypes } from "./RfpActionTypes";

export interface StoreRfpsAction {
    type: RfpActionTypes.STORE_RFPS;
    rfps: ReportsModel[];
 }
 
 export const storeRfpsActionCreator = (rfps: ReportsDataModel): StoreRfpsAction => {
    return {
       type: RfpActionTypes.STORE_RFPS,
       rfps: reports.reports,
    };
 };
 