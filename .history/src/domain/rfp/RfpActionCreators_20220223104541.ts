import { createAction } from "@reduxjs/toolkit";
import { RfpActionTypes } from "./RfpActionTypes";
import { RfpModel } from "./RfpModels";

export const storeRfpsActionCreator = createAction<RfpModel[]>(RfpActionTypes.STORE_RFPS);