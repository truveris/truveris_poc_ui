import { RootState } from "../../store/store";

const local = (state: RootState) => state.rfpReducer;

export const rfpsSelector = (state: RootState) => local(state).rfps;