import { LocalDate } from "@js-joda/core";
import { PropSchema, serializable } from "serializr";

export const toStringSerializer = (sourcePropertyValue?: object): string | undefined => {
   return sourcePropertyValue && sourcePropertyValue.toString();
};


export class RfpModel {
    @serializable
    id: number;

    @serializable
    awarded_at: string;
 
    @serializable
    client_name: string;

    @serializable
    contract_pbm: string;

    @serializable
    implementation_date: LocalDate;

    @serializable
    next_milestone: LocalDate;

    @serializable
    rfp_sent: LocalDate;

    @serializable
    notes: string;

    @serializable
    rfp_status: string;

    @serializable
    year: string;
 }