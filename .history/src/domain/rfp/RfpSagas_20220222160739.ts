import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import { fetchRfpsAdapter } from '../../adapters/RfpAdapter';
import { RfpActionTypes } from './RfpActionTypes';
import { RfpModel } from './RfpModels';

export function* getRfpsSaga(): SagaIterator {
   const urlRfps = "/reporter/api/reports";

   const rfps: RfpModel[] = yield call(fetchRfpsAdapter, urlRfps);

   yield put(storeRfps(rfps)); // TODO
}

export function* watchRfpsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(RfpActionTypes.FETCH_RFPS, getRfpsSaga),
             ]);
}

function storeRfps(rfps: RfpModel[]): any {
   throw new Error('Function not implemented.');
}
