import { createSlice } from '@reduxjs/toolkit';
import { RfpModel } from './RfpModels';

export interface ReportsReducerState {
    rfps: RfpModel[];
}

const initialState: ReportsReducerState = {
  rfps: [],
};

