import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import { RfpActionTypes } from './RfpActionTypes';

export function* getRfpsSaga(action: UpdateReportsAction): SagaIterator {
   yield put(updateReportsReceivedActionCreator(false));

   const urlReports = "/reporter/api/reports";

   const reports: ReportsDataModel = yield call(getReportsAdapter, urlReports);

   yield put(storeReportsActionCreator(reports));
}

export function* watchReportsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(RfpActionTypes.FETCH_RFPS,
                           saga(getReportsSaga)),
             ]);
}