import { createReducer } from '@reduxjs/toolkit';
import { storeRfpsActionCreator } from './RfpActionCreators';
import { RfpModel } from './RfpModels';

export interface ReportsReducerState {
    rfps: RfpModel[];
}

const initialState: ReportsReducerState = {
  rfps: [],
};

const rfpReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(storeRfpsActionCreator, (state: ReportsReducerState, action: Pay) => {
      state.value++
    })
})

// ****createSlice example****
// export const rfpsSlice = createSlice({
//   name: 'rfpSlice',
//   initialState,
//   reducers: {
//     storeRfpsAction: (state, action) => {
//         return {
//            rfps: action.payload
//         };
//       }
//   }
// });

// export const { storeRfpsAction } = rfpsSlice.actions;

// export default rfpsSlice.reducer;