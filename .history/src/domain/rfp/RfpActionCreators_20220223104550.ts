import { createAction } from "@reduxjs/toolkit";
import { RfpActionTypes } from "./RfpActionTypes";
import { RfpModel } from "./RfpModels";

export const storeRfpsActionCreator = createAction<RfpModel[]>(RfpActionTypes.STORE_RFPS);


// export interface StoreRfpsAction {
//     type: RfpActionTypes.STORE_RFPS;
//     rfps: RfpModel[];
//  }
 
//  export const storeRfpsActionCreator = (rfps: RfpModel[]): StoreRfpsAction => {
//     return {
//        type: RfpActionTypes.STORE_RFPS,
//        rfps: rfps,
//     };
//  };