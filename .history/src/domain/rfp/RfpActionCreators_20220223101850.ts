import { RfpActionTypes } from "./RfpActionTypes";

export interface StoreReportsAction {
    type: RfpActionTypes.STORE_REPORTS;
    rfps: ReportsModel[];
 }
 
 export const storeReportsActionCreator = (reports: ReportsDataModel): StoreReportsAction => {
    return {
       type: ReportsActionTypes.STORE_REPORTS,
       reports: reports.reports,
       selfLink: reports.selfLink
    };
 };
 