export enum RfpActionTypes {
    FETCH_RFPS = 'FETCH_RFPS',
    STORE_RFPS = 'STORE_RFPS',
}