import { serializable } from "serializr";

export class TagsModel {
    @serializable
    awarded_at: string;
 
    @serializable
    client_name: string;

    @serializable
    id: nu;
 }