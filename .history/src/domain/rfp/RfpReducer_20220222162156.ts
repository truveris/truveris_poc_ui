import { createSlice } from '@reduxjs/toolkit';
import { RfpModel } from './RfpModels';

export interface ReportsReducerState {
    rfps: RfpModel[];
}

const initialState: ReportsReducerState = {
  rfps: [],
};

export const rfpsSlice = createSlice({
  name: 'rfpSlice',
  initialState,
  reducers: {
    storeRfpsAction: (state, action) => {
        return {
            rfps: action.payload
        };
      }
  }
});

export const { storeRfps } = rfpsSlice.actions;

export default rfpsSlice.reducer;