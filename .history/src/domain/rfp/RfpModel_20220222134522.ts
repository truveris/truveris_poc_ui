import { serializable } from "serializr";

export class TagsModel {
    @serializable
    awarded_at: string;
 
    @serializable
    label: string;
 }