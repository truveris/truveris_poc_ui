import { serializable } from "serializr";

export class TagsModel {
    @serializable
    id: number;

    @serializable
    awarded_at: string;
 
    @serializable
    client_name: string;

    @serializable
    contract_pbm: string;
    @serializable
    implementation_date: date;

 }