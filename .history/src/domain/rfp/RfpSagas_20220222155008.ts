import {SagaIterator} from 'redux-saga';
import {all, call, put, takeLatest} from 'redux-saga/effects';
import { fetchRfpsAdapter } from '../../adapters/RfpAdapter';
import { RfpActionTypes } from './RfpActionTypes';

export function* getRfpsSaga(): SagaIterator {
   const urlRfps = "/reporter/api/reports";

   const reports: ReportsDataModel = yield call(fetchRfpsAdapter, urlReports);

   yield put(storeReportsActionCreator(reports));
}

export function* watchReportsAsyncActions(): SagaIterator {
   yield all([
                takeLatest(RfpActionTypes.FETCH_RFPS, getRfpsSaga),
             ]);
}