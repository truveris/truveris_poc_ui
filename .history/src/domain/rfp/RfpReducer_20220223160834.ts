import { ActionReducerMapBuilder, createReducer, PayloadAction } from '@reduxjs/toolkit';
import { storeRfpsActionCreator } from './RfpActionCreators';
import { RfpModel } from './RfpModels';

export interface RfpReducerState {
    rfps: RfpModel[];
}

const initialState: RfpReducerState = {
  rfps: [],
};

const handleStoreRfps = (state: RfpReducerState, action: PayloadAction<RfpModel[]>) => {
  return {
     rfps: action.payload,
  };
};

export const rfpReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(storeRfpsActionCreator, handleStoreRfps)
})

// ****createSlice example****
// export const rfpsSlice = createSlice({
//   name: 'rfpSlice',
//   initialState,
//   reducers: {
//     storeRfpsAction: (state, action) => {
//         return {
//            rfps: action.payload
//         };
//       }
//   }
// });

// export const { storeRfpsAction } = rfpsSlice.actions;

// export default rfpsSlice.reducer;