
export interface StoreReportsAction {
    type: ReportsActionTypes.STORE_REPORTS;
    reports: ReportsModel[];
 }
 
 export const storeReportsActionCreator = (reports: ReportsDataModel): StoreReportsAction => {
    return {
       type: ReportsActionTypes.STORE_REPORTS,
       reports: reports.reports,
       selfLink: reports.selfLink
    };
 };
 