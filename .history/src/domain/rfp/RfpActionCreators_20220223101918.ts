import { RfpActionTypes } from "./RfpActionTypes";

export interface StoreRfpsAction {
    type: RfpActionTypes.STORE_RFPS;
    rfps: ReportsModel[];
 }
 
 export const storeRfpsActionCreator = (reports: ReportsDataModel): StoreRfpsAction => {
    return {
       type: ReportsActionTypes.STORE_REPORTS,
       reports: reports.reports,
       selfLink: reports.selfLink
    };
 };
 