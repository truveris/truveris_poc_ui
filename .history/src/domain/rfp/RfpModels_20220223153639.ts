import { LocalDate } from "@js-joda/core";
import { serializable } from "serializr";

export class RfpModel {
    @serializable
    id: number;

    @serializable
    awarded_at: string;
 
    @serializable
    client_name: string;

    @serializable
    contract_pbm: string;

    @serializable
    implementation_date: LocalDate;

    @serializable
    next_milestone: Date;

    @serializable
    rfp_sent: Date;

    @serializable
    notes: string;

    @serializable
    rfp_status: string;

    @serializable
    year: string;
 }