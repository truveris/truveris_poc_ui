import React from 'react';
import { render, screen } from '@testing-library/react';
import { App } from './App';

test('renders wrapper div', () => {
  const { container } = render(<App/>)

  const wrapper = container.getElementsByClassName('app-wrapper')
  expect((wrapper).length;
});
