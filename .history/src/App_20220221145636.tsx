import React from 'react';
import logo from './logo.svg';
import './App.css';

export const App = () => {
  return (
    <div className="app-wrapper">
      <header className="App-header">
        <img src={logo} className="logo" alt="logo" />
      </header>

      <div>
        test
      </div>

    </div>
  );
}
