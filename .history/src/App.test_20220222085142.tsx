import React from 'react';
import { render, screen } from '@testing-library/react';
import { App } from './App';

test('renders wrapper div', () => {
  const { container } = render(<Button variant="default" />)
  render(<App />);

  const wrapper = document.getElementsByClassName('app-wrapper')
  expect(wrapper).toBeInTheDocument();
});
