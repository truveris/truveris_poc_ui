import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './app-store/store';
import { App } from './App';

test('renders title', () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  const title = getByText(/Truveris POC/i);
  expect(linkElement).toBeInTheDocument();
});
