import React from 'react';

test('renders grid with no data', async () => {
  renderWithRedux(
    <TableComponent />, {store, initialState}
  );

  expect(screen.getByRole('grid')).toBeInTheDocument();
  expect(screen.getByRole('grid')).toHaveTextContent('No rows');
});

// TODO add more test if row rendering issues get fixed https://github.com/mui/mui-x/issues/1151