import React from "react";
import 'jest';
import { saveToLocalStorage } from "../LocalStorage";

describe("saveToLocalStorage", () => {


  test("saveToLocalStorage should save value to local storage", () => {

    Object.defineProperty(window, "localStorage", {
        value: {
          getItem: jest.fn(() => null),
          setItem: jest.fn(() => null)
        },
        writable: true
      });

      saveToLocalStorage('jwtTest', '1234');
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);
      expect(window.localStorage.setItem).toHaveBeenCalledWith(
        "name",
        '"Daniel"'
      );
  });
});

