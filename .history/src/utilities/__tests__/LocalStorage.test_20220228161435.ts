import React from "react";
import 'jest';
import { saveJwtToLocalStorage } from "../LocalStorage";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("saveJwtToLocalStorage", () => {


  test("saveJwtToLocalStorage should save value to local storage", () => {
    const mockWindow = {
        location: {
           search: '?jwt=123'
        }
     } as Window;
     overrideWindow(mockWindow);

     const localStorageMock = {
        getItem: jest.fn(),
        setItem: jest.fn(),
        clear: jest.fn()
      };
      // @ts-ignore
      global.localStorage = localStorageMock;
     
      expect(localStorage.getItem).toBeCalledWith('token')
  });
});

