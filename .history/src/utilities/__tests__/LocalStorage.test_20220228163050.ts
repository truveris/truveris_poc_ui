import React from "react";
import 'jest';
import { saveToLocalStorage } from "../LocalStorage";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("saveJwtToLocalStorage", () => {


  test("saveJwtToLocalStorage should save value to local storage", () => {

     const localStorageMock = {
        getItem: jest.fn(),
        setItem: jest.fn(),
        clear: jest.fn()
      };
      // @ts-ignore
      global.localStorage = localStorageMock;

      saveJwtToLocalStorage();
      expect(localStorageMock.setItem()).toHaveBeenCalled();
  });
});

