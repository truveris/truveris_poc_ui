import React from "react";
import 'jest';
import { saveJwtToLocalStorage } from "../LocalStorage";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("saveJwtToLocalStorage", () => {


  test("saveJwtToLocalStorage should save value to local storage", () => {
    const mockWindow1 = {
        location: {
           search: '?jwtTest=123'
        }
     } as Window;
     overrideWindow(mockWindow1);

     const localStorageMock = {
        getItem: jest.fn(),
        setItem: jest.fn(),
        clear: jest.fn()
      };
      // @ts-ignore
      global.localStorage = localStorageMock;

      
      expect(localStorageMock.getItem('jwtTest')).toBe('123');
  });
});

