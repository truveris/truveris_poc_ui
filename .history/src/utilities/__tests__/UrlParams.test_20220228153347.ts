import React from 'react';
import { getParamFromUrl } from '../UrlParams';

global.window = Object.create(window);
const url = "http://dummy.com";
Object.defineProperty(window, "location", {
    value: {
       href: url
    },
    writable: true
});

describe("getParamFromUrl", () => {
    test('getParamFromUrl should return param value based on input', () => {
      expect(getParamFromUrl).toBe(3);
    });
   })