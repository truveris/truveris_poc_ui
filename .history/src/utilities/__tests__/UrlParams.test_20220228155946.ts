import React from "react";
import 'jest';
import { getParamFromUrl } from "../UrlParams";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("getParamFromUrl", () => {


  test("getParamFromUrl should return correct param value based arguments", () => {
    const mockWindow = {
        location: {
           search: '?jwtTest=123'
        }
     } as Window;
     overrideWindow(mockWindow);
     
     expect(getParamFromUrl(mockWindow.location.search, 'jwtTest')).toBe("123");
  });

  test("getParamFromUrl should return null if param does not exist", () => {
    const mockWindow = {
        location: {
           search: '?jwtTest=123'
        }
     } as Window;
     overrideWindow(mockWindow);
     
     expect(getParamFromUrl(mockWindow.location.search, '')).toBe("123");
  });
});

