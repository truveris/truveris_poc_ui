import React from "react";
import { getParamFromUrl } from "../UrlParams";

describe("getParamFromUrl", () => {
    const savedLocation = window.location;

    beforeEach(() => {
        // @ts-ignore
        delete window.location;
        // @ts-ignore
        window.location = Object.assign(new URL("https://example.org/jwtTest=123"), {
          ancestorOrigins: "",
          assign: jest.fn(),
          reload: jest.fn(),
          replace: jest.fn()
        });
      });
      afterEach(() => {
        window.location = savedLocation;
      });

  test("getParamFromUrl should return param value based on input", () => {
      console.log(getParamFromUrl('jwtTest'))
    expect(getParamFromUrl('jwtTest')).toBe(123);
  });
});

