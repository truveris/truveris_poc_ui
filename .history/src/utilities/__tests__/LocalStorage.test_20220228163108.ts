import React from "react";
import 'jest';
import { saveToLocalStorage } from "../LocalStorage";

describe("saveToLocalStorage", () => {


  test("saveToLocalStorage should save value to local storage", () => {

     const localStorageMock = {
        getItem: jest.fn(),
        setItem: jest.fn(),
        clear: jest.fn()
      };
      // @ts-ignore
      global.localStorage = localStorageMock;

      saveJwtToLocalStorage('');
      expect(localStorageMock.setItem()).toHaveBeenCalled();
  });
});

