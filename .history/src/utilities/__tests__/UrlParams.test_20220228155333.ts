import React from "react";
import 'jest';
import { getParamFromUrl } from "../UrlParams";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("getParamFromUrl", () => {
    beforeEach(() => {
        overrideWindow(null);
     });

  test("getParamFromUrl should return param value based on input", () => {
    const mockWindow = {
        location: {
            href: 
           search: 'jwt=123'
        }
     } as Window;
     overrideWindow(mockWindow);
     
     console.log(mockWindow);

     expect(getParamFromUrl('jwtTest')).toBe(123);
  });
});

