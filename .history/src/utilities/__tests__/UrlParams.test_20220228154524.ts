import React from "react";
import { getParamFromUrl } from "../UrlParams";

describe("getParamFromUrl", () => {
    beforeEach(() => {
        // @ts-ignore
        delete window.location;
        window.location = Object.assign(new URL("https://example.org"), {
          ancestorOrigins: "",
          assign: jest.fn(),
          reload: jest.fn(),
          replace: jest.fn()
        });
      });
      afterEach(() => {
        window.location = savedLocation;
      });

  test("getParamFromUrl should return param value based on input", () => {
    expect(getParamFromUrl('jwtTest')).toBe(123);
  });
});
