import React from "react";
import 'jest';
import { saveJwtToLocalStorage } from "../LocalStorage";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("saveJwtToLocalStorage", () => {


  test("saveJwtToLocalStorage should save value to local storage", () => {
    const mockWindow = {
        location: {
           search: '?jwt=123'
        }
     } as Window;
     overrideWindow(mockWindow);
     
     expect(getParamFromUrl(mockWindow.location.search, 'jwtTest')).toBe("123");
  });
});

