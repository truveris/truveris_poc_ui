import React from "react";
import 'jest';
import { getParamFromUrl } from "../UrlParams";

describe("getParamFromUrl", () => {
    beforeEach(() => {
        overrideWindow(null);
     });

    const mockWindow = {
        location: {
           search: 'locale=fr-FR'
        },
        navigator: {
           language: 'hr-HR'
        }
     } as Window;
     overrideWindow(mockWindow);

  test("getParamFromUrl should return param value based on input", () => {
      console.log(getParamFromUrl('jwtTest'));
    expect(getParamFromUrl('jwtTest')).toBe(123);
  });
});

