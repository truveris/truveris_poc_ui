import React from 'react';
import { screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { store } from '../../../store/store';
import { renderWithRedux } from '../../../utilities/TestUtils';
import { initialState } from '../../../domain/rfp/RfpReducer';
import { DataGridProps } from '@mui/x-data-grid';

// const rfps: RfpModel[] = [{    
//   id: 1,
//   awarded_at: "awarded_at",
//   client_name: "client_name",
//   contract_pbm: "string",
//   implementation_date: "2021-01-27",
//   next_milestone: "2021-01-27",
//   rfp_sent: "2021-01-27",
//   notes: "note 123",
//   rfp_status: "awarded",
//   year: "2022"
// },
// {    
//   id: 2,
//   awarded_at: "awarded_at",
//   client_name: "client_name",
//   contract_pbm: "string",
//   implementation_date: "2021-01-27",
//   next_milestone: "2021-01-27",
//   rfp_sent: "2021-01-27",
//   notes: "note 321",
//   rfp_status: "awarded",
//   year: "2021"
// }
// ];

jest.mock('@mui/x-data-grid', () => {
  const { DataGrid } = jest.requireActual('@mui/x-data-grid');
  return {
    ...jest.requireActual('@mui/x-data-grid'),
    DataGrid: (props: DataGridProps) => {
      return (
        <DataGrid
          {...props}
          autoHeight
        />
      );
    },
  };
});

test('renders grid with no data', async () => {
  renderWithRedux(
    <TableComponent />, {store, initialState}
  );

  expect(screen.getByRole('grid')).toBeInTheDocument();
  expect(screen.getByRole('grid')).toHaveTextContent('No rows');
});

// TODO add more test if row rendering issues get fixed https://github.com/mui/mui-x/issues/1151