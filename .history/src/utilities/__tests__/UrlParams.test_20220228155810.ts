import React from "react";
import 'jest';
import { getParamFromUrl } from "../UrlParams";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("getParamFromUrl", () => {


  test("getParamFromUrl should return param value based on input", () => {
      
     expect(getParamFromUrl('jwtTest')).toBe("123");
  });
});

