import React from "react";
import 'jest';
import { getParamFromUrl } from "../UrlParams";
let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
    windowOverride = window;
 };

describe("getParamFromUrl", () => {
    beforeEach(() => {
        overrideWindow(null);
     });



  test("getParamFromUrl should return param value based on input", () => {
      console.log(getParamFromUrl('jwtTest'));
    expect(getParamFromUrl('jwtTest')).toBe(123);
  });
});

