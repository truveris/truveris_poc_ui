import React from "react";
import { getParamFromUrl } from "../UrlParams";

describe("getParamFromUrl", () => {
  beforeEach(() => {
    global.window = Object.create(window);
    const url = "http://dummy.com/?jwtTest=123";
    Object.defineProperty(window, "location", {
      value: {
        href: url,
      },
      writable: true,
    });
  });

  test("getParamFromUrl should return param value based on input", () => {
    expect(getParamFromUrl('jwt')).toBe(123);
  });
});
