import React from "react";
import 'jest';
import { saveToLocalStorage } from "../LocalStorage";

describe("saveToLocalStorage", () => {


  test("saveToLocalStorage should save value to local storage", () => {

     const localStorageMock = {
        getItem: jest.fn(),
        setItem: jest.fn(),
        clear: jest.fn()
      };
      // @ts-ignore
      global.localStorage = localStorageMock;

      saveToLocalStorage('jwtTest', '1234');
      expect(localStorageMock.getItem('jwtTest')).toBeDefined();
  });
});

