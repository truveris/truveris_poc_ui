import React from 'react';
import { getParamFromUrl } from '../UrlParams';

describe("getParamFromUrl", () => {
    test('getParamFromUrl should return param value based on input', () => {
      expect(getParamFromUrl(window.location.search, '')).toBe(3);
    });
   })