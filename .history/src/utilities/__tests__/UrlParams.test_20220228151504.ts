import React from 'react';
import { getParamFromUrl } from '../UrlParams';

describe("getParamFromUrl", () => {
    test('getParamFromUrl should return param value based on input', () => {
      expect(getParamFromUrl.sum(1, 2)).toBe(3);
    });
   })