import { createTheme, Theme } from '@mui/material/styles'

export const lightTheme: Theme = createTheme({
  palette: {
    mode: 'light',
    text: {
      primary: "#181818"
    },
  },
});

export const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });