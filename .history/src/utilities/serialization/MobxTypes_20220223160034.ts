const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 