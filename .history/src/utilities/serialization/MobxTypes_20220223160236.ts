import { LocalDate } from "@js-joda/core";
import { PropSchema } from "serializr";
import { namedClassWithParseFnDeserializer } from "./NamedClassWithParseFnDeserializer";
import { toStringSerializer } from "./ToStringSerializer";

const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 