import { PropSchema } from "serializr";
import { namedClassWithParseFnDeserializer } from "./NamedClassWithParseFnDeserializer";

const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 