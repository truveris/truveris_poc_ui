export const toStringSerializer = (sourcePropertyValue?: object): string | undefined => {
    return sourcePropertyValue && sourcePropertyValue.toString();
 };