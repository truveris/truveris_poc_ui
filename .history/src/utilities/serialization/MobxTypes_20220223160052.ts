import { toStringSerializer } from "../../domain/rfp/RfpModels";
import { namedClassWithParseFnDeserializer } from "./NamedClassWithParseFnDeserializer";

const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 