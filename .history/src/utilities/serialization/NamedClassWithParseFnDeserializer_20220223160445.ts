
export type NamedClassWithParseFn = {
   name: string;
   parse: Function;
}

const parseExceptionMessage = (jsonValue: string, className: string) => {
   return `Failed to parse ${jsonValue} into ${className}`;
};

export const namedClassWithParseFnDeserializer = <T extends NamedClassWithParseFn>(className: T) => {
   return (jsonValue: string, done: MobxDoneCallback) => {
      try {
         const parsedValue = jsonValue ? className.parse(jsonValue) : null;
         return done(null, parsedValue);
      }
      catch (error) {
         return done(parseExceptionMessage(jsonValue, className.name), jsonValue);
      }
   };
};
