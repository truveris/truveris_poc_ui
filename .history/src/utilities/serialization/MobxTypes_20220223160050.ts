import { toStringSerializer } from "../../domain/rfp/RfpModels";
import { namedClassWithParseFnDeserializer } from "./NamedClassWithParseFnDeserializer";

type NewType = PropSchema;

const localDate = (): NewType => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 