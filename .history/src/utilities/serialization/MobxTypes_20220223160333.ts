import { LocalDate, LocalDateTime } from "@js-joda/core";
import { PropSchema } from "serializr";
import { namedClassWithParseFnDeserializer } from "./NamedClassWithParseFnDeserializer";
import { toStringSerializer } from "./ToStringSerializer";

/**
 * Pass this function as argument to serializable decorator to convert property to a LocalDate object
 * during deserialization and back to a LocalDate string during serialization.
 *
 * ```typescript
 * class TestModel {
 *    @serializable(localDate())
 *    property: LocalDate;
 * }
 * ```
 */
export const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 
 /**
 * Pass this function as argument to serializable decorator to convert property to a LocalDateTime object
 * during deserialization and back to a LocalDateTime string during serialization.
 *
 * ```typescript
 * class TestModel {
 *    @serializable(localDateTime())
 *    property: LocalDateTime;
 * }
 * ```
 */
export const localDateTime = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDateTime)
 });

 /**
 * Pass this function as argument to serializable decorator to convert property to a LocalTime object
 * during deserialization and back to a LocalTime string during serialization.
 *
 * ```typescript
 * class TestModel {
 *    @serializable(localTime())
 *    property: LocalTime;
 * }
 * ```
 */
const localTime = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalTime)
 });