import { LocalDate } from "@js-joda/core";
import { PropSchema } from "serializr";
import { namedClassWithParseFnDeserializer } from "./NamedClassWithParseFnDeserializer";
import { toStringSerializer } from "./ToStringSerializer";

/**
 * Pass this function as argument to serializable decorator to convert property to a LocalDate object
 * during deserialization and back to a LocalDate string during serialization.
 *
 * ```typescript
 * class TestModel {
 *    @serializable(localDate())
 *    property: LocalDate;
 * }
 * ```
 */
const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 