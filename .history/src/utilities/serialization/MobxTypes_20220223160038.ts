import { toStringSerializer } from "../../domain/rfp/RfpModels";

const localDate = (): PropSchema => ({
    serializer: toStringSerializer,
    deserializer: namedClassWithParseFnDeserializer(LocalDate)
 });
 