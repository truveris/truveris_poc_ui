import { getParamFromUrl } from "../utilities/UrlParams";

export const saveJToLocalStorage = () => {
    const jwt: string | null = getParamFromUrl(window.location.search, 'jwt');

    if (jwt) {
      localStorage.setItem('jwt', jwt);
    }
}
