import React, { ReactElement, ReactNode } from "react";
import { render as rtlRender, RenderOptions } from "@testing-library/react";
import { DynamicModuleLoader } from "redux-dynamic-modules";
import { Provider } from "react-redux";
import { reduxSearch } from "../redux/search";
import { store, history } from "../redux/store";
import { Router } from "react-router-dom";

export interface WrapperProps {
  children: ReactElement;
}

const render = (ui: ReactElement, renderOptions?: RenderOptions) => {
  const Wrapper = ({ children }: WrapperProps): ReactElement => {
    return (
      <Provider store={store}>
        <Router history={history}>
          <DynamicModuleLoader
            modules={[reduxSearch()]}
            createStore={() => store}
          >
            {children}
          </DynamicModuleLoader>
        </Router>
      </Provider>
    );
  };
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
};

// re-export everything
export * from "@testing-library/react";

// override render method
expo