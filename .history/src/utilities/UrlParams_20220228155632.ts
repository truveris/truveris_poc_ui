export const getParamFromUrl = (windowSearchParam: param: string): string | null => {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(param);
}