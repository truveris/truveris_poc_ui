import { createTheme, Theme } from '@mui/material/styles'

export const lightTheme: Theme = createTheme({
  palette: {
    primary: {
      
    }
    mode: 'light',
    text: {
      primary: "#391960"
    },
  },
});

export const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });