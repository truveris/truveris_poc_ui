import { DateTimeFormatter, LocalDate } from "@js-joda/core";

export const formatDate = (dateString: string) => {
    const date = LocalDate.parse(dateString);

    return date.format(DateTimeFormatter.ofPattern('MM/dd/yyyy'));
};

export const formatDate = (dateString: string) => {
    const date = LocalDate.parse(dateString);

    return date.format(DateTimeFormatter.ofPattern('MM/dd/yyyy'));
};