import { createTheme } from '@mui/material/styles'

export const theme = createTheme({
  palette: {
    text: {
      primary: "#391960"
    }
  }
});
