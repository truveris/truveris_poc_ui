import { createTheme, Theme } from '@mui/material/styles'

export const theme: Theme = createTheme({
  palette: {
    text: {
      primary: "#391960"
    },
  },
});
