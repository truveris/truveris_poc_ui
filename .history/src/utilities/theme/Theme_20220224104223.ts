import { createMuiTheme } from "@mui/material";

const theme = createMuiTheme({
  palette: {
    text: {
      primary: "#391960"
    }
  }
});

export default theme;
