import { createTheme } from '@mui/material/styles'

export const theme: Theme = createTheme({
  palette: {
    text: {
      primary: "#391960"
    }
  }
});
