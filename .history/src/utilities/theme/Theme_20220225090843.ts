import { createTheme, Theme } from '@mui/material/styles'

export const lightTheme: Theme = createTheme({
  palette: {
    text: {
      primary: "#391960"
    },
  },
});

export const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });