import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  palette: {
    text: {
      primary: "#391960"
    }
  }
});

export default theme;
