import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
  palette: {
    text: {
      primary: "#391960"
    }
  }
});

export default theme;
