import { createTheme } from '@mui/material/styles'

const theme = createMuiTheme({
  palette: {
    text: {
      primary: "#391960"
    }
  }
});

export default theme;
