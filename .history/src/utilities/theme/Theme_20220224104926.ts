import { createTheme, Theme } from '@mui/material/styles'

export const theme: Theme = createTheme({
  palette: {
    text: {
      primary: "#391960"
    },
    typography: {
        // In Chinese and Japanese the characters are usually larger,
        // so a smaller fontsize may be appropriate.
        fontSize: 12,
      }
  }
});
