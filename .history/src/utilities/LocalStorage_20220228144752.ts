import { getParamFromUrl } from "./utilities/UrlParams";
if (jwt) {
  localStorage.setItem('jwt', jwt);
}


export const saveJwtToLocalStorage = () => {
    const jwt: string | null = getParamFromUrl('jwt');

    if (jwt) {
      localStorage.setItem('jwt', jwt);
    }
}
