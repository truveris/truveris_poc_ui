import { createTheme, Theme } from '@mui/material/styles'

export const lightTheme: Theme = createTheme({
  palette: {
    primary: {
      light: '#757ce8',
      main: '#3f50b5',
      dark: '#002884',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
    mode: 'light',
    text: {
      primary: "#391960"
    },
  },
});

export const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });