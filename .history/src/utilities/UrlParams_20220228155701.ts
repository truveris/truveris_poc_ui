export const getParamFromUrl = (queryString: string, param: string): string | null => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(param);
}