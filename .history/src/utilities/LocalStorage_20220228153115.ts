import { getParamFromUrl } from "../utilities/UrlParams";

export const saveJwtToLocalStorage = () => {
    const jwt: string | null = getParamFromUrl(window.location.searc, 'jwt');

    if (jwt) {
      localStorage.setItem('jwt', jwt);
    }
}
