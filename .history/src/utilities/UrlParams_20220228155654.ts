export const getParamFromUrl = (param: string): string | null => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(param);
}