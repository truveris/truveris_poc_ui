export const getParamFromUrl = (param: string): string | null => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    return urlParams.get(param);

      // if (jwt) {
    //   localStorage.setItem('jwt', jwt);
    // }
}