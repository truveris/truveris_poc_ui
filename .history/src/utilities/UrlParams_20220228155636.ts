export const getParamFromUrl = (windowSearchParam: string, param: string): string | null => {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(param);
}