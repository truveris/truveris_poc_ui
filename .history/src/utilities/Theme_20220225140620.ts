import { createTheme, Theme } from '@mui/material/styles'

export const lightTheme: Theme = createTheme({
  palette: {
    primary: {
      main: '#0971f1'
    },
    neutral: {
      main: '#64748B',
      contrastText: '#fff',
    },
});

export const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });