import { getParamFromUrl } from "../utilities/";
if (jwt) {
  localStorage.setItem('jwt', jwt);
}


export const saveJwtToLocalStorage = () => {
    const jwt: string | null = getParamFromUrl('jwt');

    if (jwt) {
      localStorage.setItem('jwt', jwt);
    }
}
