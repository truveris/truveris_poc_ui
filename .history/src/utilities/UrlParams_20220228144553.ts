const getParamFromUrl = (param: string): string | null => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    return urlParams.get(param);
}

const jwt: string | null = getParamFromUrl('jwt');

if (jwt) {
  localStorage.setItem('jwt', jwt);
}


export const save