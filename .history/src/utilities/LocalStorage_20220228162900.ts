import { getParamFromUrl } from "../utilities/UrlParams";

export const saveToLocalStorage = (name: string, value: string | null) => {
    if (value) {
      localStorage.setItem(name, value);
    }
}
