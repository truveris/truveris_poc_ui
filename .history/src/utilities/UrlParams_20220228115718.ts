const getParamFromUrl = (param: string) => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const jwt: string | null = urlParams.get('jwt');
}