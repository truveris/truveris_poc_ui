import { createTheme, Theme } from '@mui/material/styles'

export const lightTheme: Theme = createTheme({
  palette: {
    primary: {
      main: '#0971f1',
      dark: '#053e85',
    },
    mode: 'light',
    text: {
      primary: "#391960"
    },
  },
});

export const darkTheme = createTheme({
    palette: {
      mode: 'dark',
    },
  });