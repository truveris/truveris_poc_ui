import { DateTimeFormatter, LocalDate } from "@js-joda/core";

export const formatDate = (dateString: string) => {
    const date = LocalDate.parse("2023-01-01");

    return date.format(DateTimeFormatter.ofPattern('MM/dd/yyyy'));
};