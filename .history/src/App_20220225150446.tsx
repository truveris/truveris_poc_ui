import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./hooks/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { CentralSectionComponent } from "./components/central-section/CentralSectionComponent";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { darkTheme, lightTheme } from "./utilities/Theme";
import { PocContextProvider, usePocContext } from "./context/Context";

export const App = () => {
  const dispatch = useAppDispatch();
  const { dark } = usePocContext();

  useEffect(() => {
    dispatch(fetchRfpsActionCreator());
  }, [dispatch]);

  return (
    <>
      <PocContextProvider>
        <ThemeProvider theme={lightTheme}>
          <div>{dark}</div>
          <CssBaseline />
          <HeaderComponent />
          <CentralSectionComponent />
        </ThemeProvider>
      </PocContextProvider>
    </>
  );
};
