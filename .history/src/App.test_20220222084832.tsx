import React from 'react';
import { render, screen } from '@testing-library/react';
import { App } from './App';

test('renders title', () => {
  render(<App />);

  const wrapper = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
