import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch, useAppSelector } from "./store/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { RfpModel } from "./domain/rfp/RfpModels";
import { rfpsSelector } from "./domain/rfp/RfpSelectors";

export const App = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchRfpsActionCreator());
  }, [dispatch]);

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
      CentralSectionCo
    </div>
  );
};
