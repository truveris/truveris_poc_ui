import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import Container from "@mui/material/Container";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./store/hooks";
import { RfpActionTypes } from "./domain/rfp/RfpActionTypes";

export const App = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch({ type: RfpActionTypes.FETCH_RFPS })};
  }, []);

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
    </div>
  );
};
