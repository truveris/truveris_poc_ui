import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Container from "@mui/material/Container";
import { HeaderComponent } from "./components/header/HeaderComponent";

export const App = () => {
  

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
    </div>
  );
};
