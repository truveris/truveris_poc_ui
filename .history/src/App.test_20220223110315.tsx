import React from 'react';
import { render, screen } from '@testing-library/react';
import { App } from './App';
import { Provider } from 'react-redux';
import { store } from './store/store';

test('renders wrapper div', () => {
  const { container } = render(<Provider store={store}><App /></Provider>)

  const wrapper = container.getElementsByClassName('app-wrapper');
  expect((wrapper).length).toBe(1);
});
