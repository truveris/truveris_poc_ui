import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./hooks/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { CentralSectionComponent } from "./components/central-section/CentralSectionComponent";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { darkTheme, lightTheme } from "./utilities/Theme";
import { usePocContext } from "./context/Context";
import { FooterComponent } from "./components/footer/FooterComponent";
import { saveToLocalStorage } from "./utilities/LocalStorage";
import { getParamFromUrl } from "./utilities/UrlParams";

export const App = () => {
  const dispatch = useAppDispatch();
  const { dark } = usePocContext();

  useEffect(() => {
    const jwt: string | null = getParamFromUrl(window.location.search, 'jwt');
    saveToLocalStorage('jwt', getParamFromUrl(window.location.search, 'jwt'));
    
    dispatch(fetchRfpsActionCreator());
  }, [dispatch]);

  return (
    <>
        <ThemeProvider theme={dark ? darkTheme : lightTheme}>
          <CssBaseline />
          <HeaderComponent />
          <CentralSectionComponent />
          <FooterComponent />
        </ThemeProvider>
    </>
  );
};

