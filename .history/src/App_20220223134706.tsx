import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch, useAppSelector } from "./store/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { RfpModel } from "./domain/rfp/RfpModels";

export const App = () => {
  const dispatch = useAppDispatch();
  const rfps: RfpModel[] = useAppSelector(rfpsSelector)
  
  useEffect(() => {
    dispatch(fetchRfpsActionCreator());
  }, [dispatch]);

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
    </div>
  );
};
