import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./store/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { CentralSectionComponent } from "./components/central-section/CentralSectionComponent";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { theme } from "./utilities/theme/Theme";

export const App = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchRfpsActionCreator());
  }, [dispatch]);

  return (
    <>
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <HeaderComponent/>
      <CentralSectionComponent/>
    </ThemeProvider>
    </>
  );
};
