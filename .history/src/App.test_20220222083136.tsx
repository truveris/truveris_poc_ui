import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './app-store/store';
import { App } from './App';

test('renders title', () => {
  render(<App />);

  const title = getByText(/Truveris POC/i);
  expect(title).toBeInTheDocument();
});
