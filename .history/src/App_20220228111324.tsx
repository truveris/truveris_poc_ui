import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./hooks/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { CentralSectionComponent } from "./components/central-section/CentralSectionComponent";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { darkTheme, lightTheme } from "./utilities/Theme";
import { usePocContext } from "./context/Context";
import { FooterComponent } from "./components/footer/FooterComponent";

export const App = () => {
  const { dark } = usePocContext();

  useEffect(() => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const product = urlParams.get('product')
  }, []);

  return (
    <>
        <ThemeProvider theme={dark ? darkTheme : lightTheme}>
          <CssBaseline />
          <HeaderComponent />
          <CentralSectionComponent />
          <FooterComponent />
        </ThemeProvider>
    </>
  );
};
