import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./store/hooks";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";
import { CentralSectionComponent } from "./components/central-section/CentralSectionComponent";
import { CssBaseline, PaletteMode, ThemeProvider } from "@mui/material";
import { darkTheme } from "./utilities/theme/Theme";

export const App = () => {
  const dispatch = useAppDispatch();
  const [mode, setMode] = React.useState<PaletteMode>('light');
  const colorMode = React.useMemo(
    () => ({
      // The dark mode switch would invoke this method
      toggleColorMode: () => {
        setMode((prevMode: PaletteMode) =>
          prevMode === 'lightTheme' ? 'dark' : 'light',
        );
      },
    }),
    [],
  );

  // Update the theme only if the mode changes
  const theme = React.useMemo(() => createTheme(getDesignTokens(mode)), [mode]);


  useEffect(() => {
    dispatch(fetchRfpsActionCreator());
  }, [dispatch]);

  return (
    <>
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <HeaderComponent/>
      <CentralSectionComponent/>
    </ThemeProvider>
    </>
  );
};
