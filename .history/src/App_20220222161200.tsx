import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Container from "@mui/material/Container";
import { HeaderComponent } from "./components/header/HeaderComponent";

export const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
     dispatch(getReportsActionCreator());
     // eslint-disable-next-line
  }, []);

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
    </div>
  );
};
