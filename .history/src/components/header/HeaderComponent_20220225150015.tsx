import React from "react";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";
import { usePocContext } from "../../context/Context";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Switch from "@mui/material/Switch";

export const HeaderComponent = () => {
  const { toggleDarkTheme } = usePocContext();
  const { dark } = usePocContext();

  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />
      <div>
      Dark mode <Switch onChange={toggleDarkTheme}/>
      </div>
    </div>
  );
};
