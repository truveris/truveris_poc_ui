import React from "react";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";
import { usePocContext } from "../../context/Context";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';

export const HeaderComponent = () => {
  const { toggleDarkTheme } = usePocContext();
  const { dark } = usePocContext();

  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />
      <Switch {...label} />
    </div>
  );
};
