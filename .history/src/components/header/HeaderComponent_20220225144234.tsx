import React from "react";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";
import { usePocContext } from "../../context/Context";
import Button from "@mui/material/Button";

export const HeaderComponent = () => {
  const { toggleDarkTheme } = usePocContext();

  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />
      <Button on></Button>
    </div>
  );
};
