import React from 'react';
import logo from './logo.svg';
import './App.css';

export const HeaderComponent = () => {
  return (
      <header className="app-header">
        <img src={logo} className="logo" alt="logo" />
      </header>
  );
}
