import React from 'react';
import logo from './a;
import './App.css';

export const HeaderComponent = () => {
  return (
      <header className="app-header">
        <img src={logo} className="logo" alt="logo" />
      </header>
  );
}
