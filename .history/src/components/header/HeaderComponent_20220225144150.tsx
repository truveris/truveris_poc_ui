import React from "react";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";

export const HeaderComponent = () => {
  const { toggleModalVisibility } = usePocContext();

  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />
    </div>
  );
};
