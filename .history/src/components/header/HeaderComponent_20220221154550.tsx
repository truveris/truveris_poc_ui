import React from 'react';
import logo from '../../logo.s';
import './Hea.css';

export const HeaderComponent = () => {
  return (
      <header className="app-header">
        <img src={logo} className="logo" alt="logo" />
      </header>
  );
}
