import React from "react";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";
import { usePocContext } from "../../context/Context";
import Switch from "@mui/material/Switch";

export const HeaderComponent = () => {
  const { toggleDarkTheme } = usePocContext();


  const darkModeSwitch: JSX.Element = (
    <div className="modeToggle">
    Dark mode <Switch className="dark-mode-switch" onChange={toggleDarkTheme}/>
    </div>
  );

  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />

      {darkModeSwitch}
    </div>
  );
};
