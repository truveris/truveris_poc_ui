import React from "react";
import logo from "./logo.svg";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";

export const HeaderComponent = () => {
  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />
    </div>
  );
};
