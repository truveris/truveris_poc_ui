import React from "react";
import "./HeaderComponent.css";
import { TextComponent } from "../common/TextComponent";
import { usePocContext } from "../../context/Context";
import Switch from "@mui/material/Switch";

export const HeaderComponent = () => {
  const { toggleDarkTheme } = usePocContext();
  const { dark } = usePocContext();

  return (
    <div className="header">
      <TextComponent text="Truveris POC" textVarient="h5" />

      <div className="modeToggle">
      <FormControlLabel
        control={<MaterialUISwitch sx={{ m: 1 }} defaultChecked />}
        label="MUI switch"
      />

      Dark mode <Switch onChange={() => toggleDarkTheme()}/>
      </div>
    </div>
  );
};
