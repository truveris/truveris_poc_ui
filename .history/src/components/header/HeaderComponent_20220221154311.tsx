import React from 'react';
import logo from '../../lo';
import './App.css';

export const HeaderComponent = () => {
  return (
      <header className="app-header">
        <img src={logo} className="logo" alt="logo" />
      </header>
  );
}
