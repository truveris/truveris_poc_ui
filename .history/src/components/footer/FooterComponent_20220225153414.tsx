import React from "react";
import "./FooterComponent.css";

export const FooterComponent = () => {
  return (
    <div className="footer">
      <p className="footer-note">© truvers</p>
    </div>
  );
};
