import React from "react";
import "./FooterComponent.css";

export const FooterComponent = () => {
  return (
    <div className="footer">
      © truvers
    </div>
  );
};
