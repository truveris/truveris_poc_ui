import React from "react";
import { TextComponent } from "../common/TextComponent";
import "./FooterComponent.css";

export const FooterComponent = () => {
  return (
    <div className="footer">
      
      <TextComponent className="footer-note" variant="subtitle1" text=></TextComponent>
    </div>
  );
};
