import React from "react";
import { TextComponent } from "../common/TextComponent";
import "./FooterComponent.css";

export const FooterComponent = () => {
  return (
    <div className="footer">

<TextComponent text="© truvers" textVarient="h5" />
    </div>
  );
};
