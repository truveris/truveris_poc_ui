import React from "react";
import "./FooterComponent.css";

export const FooterComponent = () => {
  return (
    <div className="footer">
      
      <TextComponent className="footer-note">© truvers</TextComponent>
    </div>
  );
};
