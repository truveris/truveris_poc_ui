import React from "react";
import { TextComponent } from "../common/TextComponent";
import "./FooterComponent.css";

export const FooterComponent = () => {
  return (
    <div className="footer">
      
      <TextComponent className="footer-note" textVarient="subtitle1" text="© truvers"/>
    </div>
  );
};
