import React, { useState } from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid, GridColumns } from '@mui/x-data-grid';
import './TableComponent.css';
import { rfpColumnDef } from "./TableHelpers";

export const TableComponent = () => {
  const [rfpsColumns] = useState<GridColumns>(rfpColumnDef);
  const rfpsRows: RfpModel[] = useAppSelector(rfpsSelector);

  return (
      <DataGrid
        data-testid="data-grid-rfps"
        rows={rfpsRows}
        columns={rfpsColumns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        autoHeight={true}
      />
  );
};
