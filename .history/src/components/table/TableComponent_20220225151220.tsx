import React, { useState } from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../hooks/hooks";
import { DataGrid, GridColumns } from '@mui/x-data-grid';
import './TableComponent.css';
import { rfpColumnDef } from "./TableHelpers";
import { usePocContext } from "../../context/Context";

export const TableComponent = () => {
  const { dark } = usePocContext();
  const [rfpsColumns] = useState<GridColumns>(rfpColumnDef);
  const rfpsRows: RfpModel[] = useAppSelector(rfpsSelector);

  return (
      <DataGrid
       className="data-grid-rfps-light"
        data-testid="data-grid-rfps"
        rows={rfpsRows}
        columns={rfpsColumns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        autoHeight={true}
      />
  );
};
