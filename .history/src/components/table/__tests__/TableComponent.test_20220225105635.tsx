import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';
import { createStore } from '@reduxjs/toolkit';
import { rfpReducer } from '../../../domain/rfp/RfpReducer';
import { RfpModel } from '../../../domain/rfp/RfpModels';

const rfp: RfpModel = [{    
  id: 1,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 123",
  rfp_status: "awarded",
  year: "2022"
}]

test('renders title', () => {
  render(
    <Provider store={createStore(rfpReducer, { rfps: [rfp] })}>
      <TableComponent />
    </Provider>
  );


  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
