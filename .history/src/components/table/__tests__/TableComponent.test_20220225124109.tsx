import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { RfpModel } from '../../../domain/rfp/RfpModels';
// @ts-ignore
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux';

const rfps: RfpModel[] = [{    
  id: 1,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 123",
  rfp_status: "awarded",
  year: "2022"
},
{    
  id: 2,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 321",
  rfp_status: "awarded",
  year: "2021"
}
];

const initialState = {output:10}
const mockStore = configureStore()
test('renders grid', () => {

  const store = mockStore(initialState)
  render(
    <Provider store={store}><App /></Provider>
  );

  expect(screen.getByText('col')).toBeInTheDocument()
});
