import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';
import { createStore } from '@reduxjs/toolkit';
import { rfpReducer } from '../../../domain/rfp/RfpReducer';
import { RfpModel } from '../../../domain/rfp/RfpModels';

const rfps: RfpModel[] = [{    
  id: 1,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 123",
  rfp_status: "awarded",
  year: "2022"
},
{    
  id: 2,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 321",
  rfp_status: "awarded",
  year: "2021"
}
]

test('renders grid', () => {
  render(
    <Provider store={createStore(rfpReducer, { rfps: rfps })}>
      <TableComponent />
    </Provider>
  );


  const grid = screen.getByTestId("data-grid-rfps");
  expect(title).toBeInTheDocument();
});
