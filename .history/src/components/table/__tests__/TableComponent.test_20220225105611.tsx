import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';
import { createStore } from '@reduxjs/toolkit';
import { rfpReducer } from '../../../domain/rfp/RfpReducer';
import { RfpModel } from '../../../domain/rfp/RfpModels';

test('renders title', () => {
  render(
    <Provider store={createStore(rfpReducer, { rfps: [rfp] })}>
      <TableComponent />
    </Provider>
  );


  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
