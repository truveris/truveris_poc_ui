import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';

test('renders title', () => {
  render(
    <Provider store={createStore(counterReducer, { count })}>
      <TableComponent />
    </Provider>
  );

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
