import React from 'react';
import { screen, waitFor } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { RfpModel } from '../../../domain/rfp/RfpModels';
import { store } from '../../../store/store';
import { renderWithRedux } from '../../../utilities/TestUtils';
import { initialState } from '../../../domain/rfp/RfpReducer';

const rfps: RfpModel[] = [{    
  id: 1,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 123",
  rfp_status: "awarded",
  year: "2022"
},
{    
  id: 2,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 321",
  rfp_status: "awarded",
  year: "2021"
}
];

test('renders grid', async () => {
  renderWithRedux(
    <TableComponent />, {store, initialState}
  );

  expect(screen.getByRole('grid')).toBeInTheDocument();
  expect(screen.getByRole('grid')).toHaveTextContent('No rows');

  
});
