import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';
import { createStore } from '@reduxjs/toolkit';
import { rfpReducer } from '../../../domain/rfp/RfpReducer';
import { RfpModel } from '../../../domain/rfp/RfpModels';

test('renders title', () => {
  render(
    <Provider store={createStore(rfpReducer, { rfps: [] })}>
      <TableComponent />
    </Provider>
  );

  const rfps: RfpModel[] = {    
    id: 1,
    awarded_at: "string",
    client_name: "string",
    contract_pbm: "string",
    implementation_date: "string",
  next_milestone: "string",
  rfp_sent: "string",
  notes: "string",
  rfp_status: "string",
  year: "string"}

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
