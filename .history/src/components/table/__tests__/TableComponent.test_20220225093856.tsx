import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';

test('renders title', () => {
  render(
    <Provider store={createStore(counterReducer, { count })}></Provider>
  );

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
