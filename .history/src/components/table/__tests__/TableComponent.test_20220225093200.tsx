import React from 'react';
import { render, screen } from '@testing-library/react';

test('renders title', () => {
  render(<Table />);

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
