import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';
import { createStore } from '@reduxjs/toolkit';
import { rfpReducer } from '../../../domain/rfp/RfpReducer';

test('renders title', () => {
  render(
    <Provider store={createStore(rfpReducer, { rfps: {    id: number,
      awarded_at: string,
    client_name: string,
    contract_pbm: string,
    implementation_date: string,
    next_milestone: string,
    rfp_sent: string,
    notes: string,
    rfp_status: string,
    year: string}})}>
      <TableComponent />
    </Provider>
  );

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
