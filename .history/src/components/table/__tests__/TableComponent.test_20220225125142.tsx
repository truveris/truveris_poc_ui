import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { RfpModel } from '../../../domain/rfp/RfpModels';
import { Provider } from 'react-redux';
import { store } from '../../../store/store';
import { DataGrid } from '@mui/x-data-grid';

const rfps: RfpModel[] = [{    
  id: 1,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 123",
  rfp_status: "awarded",
  year: "2022"
},
{    
  id: 2,
  awarded_at: "awarded_at",
  client_name: "client_name",
  contract_pbm: "string",
  implementation_date: "2021-01-27",
  next_milestone: "2021-01-27",
  rfp_sent: "2021-01-27",
  notes: "note 321",
  rfp_status: "awarded",
  year: "2021"
}
];

test('renders grid', () => {
  render(
    <Provider store={store}><TableComponent /></Provider>
    <DataGrid></DataGrid>
  );

  expect(screen.DataGrid)


});
