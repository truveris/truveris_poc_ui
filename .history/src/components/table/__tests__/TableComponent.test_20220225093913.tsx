import React from 'react';
import { render, screen } from '@testing-library/react';
import { TableComponent } from '../TableComponent';
import { Provider } from 'react-redux';
import { createStore } from '@reduxjs/toolkit';

test('renders title', () => {
  render(
    <Provider store={createStore(rfp, { count })}>
      <TableComponent />
    </Provider>
  );

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
