import React from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid, GridColumns } from '@mui/x-data-grid';
import './TableComponent.css';

export const TableComponent = () => {
  const rfps: RfpModel[] = useAppSelector(rfpsSelector);

  const rfpColumns: GridColumns = [
    { field: 'id', headerName: 'ID', width: 70, type: 'number' },
    { field: 'client_name', headerName: 'Client Name', width: 130 },
    { field: 'contract_pbm', headerName: 'Contract PBM', width: 130 },
    { field: 'awarded_at', headerName: 'Awarded at', width: 130 },
    { field: 'implementation_date', headerName: 'Implementation Date', width: 130 },
    { field: 'next_milestone', headerName: 'Next Milestone', width: 130 },
    { field: 'rfp_sent', headerName: 'RFP Sent', width: 130 },
    { field: 'rfp_status', headerName: 'RFP Status', width: 130 },
    { field: 'year', headerName: 'Year', width: 130, type: 'number' },
    { field: 'notes', headerName: 'Notes', width: 130 },
  ];

  return (
    <div className="table-wrapper">
      <DataGrid
        rows={rfps}
        columns={rfpColumns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
};
