import { GridColumns } from "@mui/x-data-grid";

export const rfpColumnDef: GridColumns = [
    { field: 'id', headerName: 'ID', type: 'number' },
    { field: 'client_name', headerName: 'Client Name'},
    { field: 'contract_pbm', headerName: 'Contract PBM'},
    { field: 'awarded_at', headerName: 'Awarded at'},
    { field: 'implementation_date', headerName: 'Implementation Date', width: 130 },
    { field: 'next_milestone', headerName: 'Next Milestone', width: 130 },
    { field: 'rfp_sent', headerName: 'RFP Sent', width: 130 },
    { field: 'rfp_status', headerName: 'RFP Status', width: 130 },
    { field: 'year', headerName: 'Year', width: 130, type: 'number' },
    { field: 'notes', headerName: 'Notes', width: 130 },
  ];