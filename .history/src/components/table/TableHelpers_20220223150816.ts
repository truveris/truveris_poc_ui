import { GridColumns } from "@mui/x-data-grid";

export const rfpColumnDef: GridColumns = [
    { field: 'id', headerName: 'ID', type: 'number' },
    { field: 'client_name', headerName: 'Client Name'},
    { field: 'contract_pbm', headerName: 'Contract PBM'},
    { field: 'awarded_at', headerName: 'Awarded at'},
    { field: 'implementation_date', headerName: 'Implementation Date'},
    { field: 'next_milestone', headerName: 'Next Milestone'},
    { field: 'rfp_sent', headerName: 'RFP Sent'},
    { field: 'rfp_status', headerName: 'RFP Status'},
    { field: 'year', headerName: 'Year', type: 'number' },
    { field: 'notes', headerName: 'Notes', width: 130 },
  ];