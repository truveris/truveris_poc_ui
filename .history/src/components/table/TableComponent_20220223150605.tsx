import React from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid, GridColumns } from '@mui/x-data-grid';
import './TableComponent.css';

export const TableComponent = () => {
  const rfps: RfpModel[] = useAppSelector(rfpsSelector);



  return (
    <div className="table-wrapper">
      <DataGrid
        rows={rfps}
        columns={rfpColumnDef}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
};
