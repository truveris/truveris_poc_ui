import React from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid } from '@mui/x-data-grid';
import './TableComponent.css';
import { rfpColumnDef } from "./TableHelpers";

export const TableComponent = () => {
  const rfpsRows: RfpModel[] = useAppSelector(rfpsSelector);

  return (
    <div className="table-wrapper">
      <DataGrid
        rows={rfpsRows}
        columns={rfpColumnDef}
        pageSize={10}
        rowsPerPageOptions={[10]}
        autoHeight={true}
      />
    </div>
  );
};
