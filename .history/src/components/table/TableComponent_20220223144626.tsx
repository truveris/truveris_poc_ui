import React from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid } from '@mui/x-data-grid';
import './TableComponent.css';

export const TableComponent = () => {
  const rfps: RfpModel[] = useAppSelector(rfpsSelector);

  const rfpColumns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'client_name', headerName: 'Client Name', width: 130 },
    { field: 'contract_pbm', headerName: 'Contract PBM', width: 130 },
    { field: 'awarded_at', headerName: 'Awarded at', width: 130 },
    { field: 'awarded_at', headerName: 'Awarded at', width: 130 },
    {
      field: 'age',
      headerName: 'Age',
      type: 'number',
      width: 90,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
    },
  ];
  "awarded_at": "2022-01-27",
  "client_name": "Canonical Inc.",
  "contract_pbm": "ExpressX International",
  "id": 13,
  "implementation_date": "2023-01-01",
  "next_milestone": "2022-01-02",
  "notes": "Awarded RFP",
  "rfp_sent": "2022-01-27",
  "rfp_status": "Awarded - ScripJimmyCo Inc.",
  "year": "2022"
  return (
    <div className="table-wrapper">
      <DataGrid
        rows={rfps}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
};
