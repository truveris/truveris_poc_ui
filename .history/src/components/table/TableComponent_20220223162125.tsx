import React from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid, GridColumns } from '@mui/x-data-grid';
import './TableComponent.css';
import { rfpColumnDef } from "./TableHelpers";
import { DateTimeFormatter, LocalDate } from "@js-joda/core";

export const TableComponent = () => {
  const rfps: RfpModel[] = useAppSelector(rfpsSelector);

  const d = LocalDate.parse("2023-01-01");

  d.format(DateTimeFormatter.ofPattern('M/d/yyyy'));


  return (
    <div className="table-wrapper">
      <div>{d.toS}</div>
      <DataGrid
        rows={rfps}
        columns={rfpColumnDef}
        pageSize={5}
        rowsPerPageOptions={[5]}
        autoHeight={true}
      />
    </div>
  );
};
