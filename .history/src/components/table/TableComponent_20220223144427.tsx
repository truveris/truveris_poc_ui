import React from "react";
import { RfpModel } from "../../domain/rfp/RfpModels";
import { rfpsSelector } from "../../domain/rfp/RfpSelectors";
import { useAppSelector } from "../../store/hooks";
import { DataGrid } from '@mui/x-data-grid';
import './TableComponent.css';

export const TableComponent = () => {
  const rfps: RfpModel[] = useAppSelector(rfpsSelector);
  const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    {
      field: 'age',
      headerName: 'Age',
      type: 'number',
      width: 90,
    },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
    },
  ];
  
  const rows = rfps;

  return (
    <div className="table-wrapper">
      <DataGrid
        rows={rfps}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
      />
    </div>
  );
};
