import { GridColumns } from "@mui/x-data-grid";
import { formatDate } from "../../utilities/DateFormatter";

export const rfpColumnDef: GridColumns = [
    { field: 'id', headerName: 'ID', maxWidth: 60, type: 'number' },
    { field: 'client_name', headerName: 'Client Name', flex: 1 },
    { field: 'contract_pbm', headerName: 'Contract PBM', flex: 1},
    { field: 'awarded_at', headerName: 'Awarded at', flex: 1, 
    valueFormatter: (params) => {
        return formatDate(params.value);
      }
    },
    { field: 'implementation_date', headerName: 'Implementation Date', flex: 1},
    { field: 'next_milestone', headerName: 'Next Milestone', flex: 1},
    { field: 'rfp_sent', headerName: 'RFP Sent', flex: 1},
    { field: 'rfp_status', headerName: 'RFP Status', flex: 1},
    { field: 'year', headerName: 'Year', type: 'number', flex: 1},
    { field: 'notes', headerName: 'Notes', flex: 1},
  ];