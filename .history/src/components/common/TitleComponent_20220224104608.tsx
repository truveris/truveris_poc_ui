import Typography from "@mui/material/Typography";
import React from "react";

interface TitleComponentProps {
    text: string;
} 

export const TitleComponent = (props: TitleComponentProps) => {
  return (
        <Typography variant="h1" component="h1">{props.text}</Typography>
  );
};
