import { Variant } from "@mui/material/styles/createTypography";
import Typography from "@mui/material/Typography";
import React from "react";

interface TitleComponentProps {
    text: string;
    textVarient: Variant;
} 

export const TextComponent = (props: TitleComponentProps) => {
  return (
        <Typography variant={props.textVarient}>{props.text}</Typography>
  );
};