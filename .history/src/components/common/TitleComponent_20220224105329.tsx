import { Variant } from "@mui/material/styles/createTypography";
import Typography from "@mui/material/Typography";
import React from "react";

interface TitleComponentProps {
    text: string;
    textVarient: Variant;
} 

export const TitleComponent = (props: TitleComponentProps) => {
  return (
        <Typography variant="h1">{props.text}</Typography>
  );
};
