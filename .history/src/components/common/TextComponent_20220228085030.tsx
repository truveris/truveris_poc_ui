import { Variant } from "@mui/material/styles/createTypography";
import Typography from "@mui/material/Typography";
import React from "react";

// example of a wrapped component with prop interface
interface TitleComponentProps {
    text: string;
    textVarient: Variant;
} 

export const TextComponent = (props: TitleComponentProps) => {
  return (
        <Typography className variant={props.textVarient}>{props.text}</Typography>
  );
};
