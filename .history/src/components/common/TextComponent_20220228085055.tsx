import { Variant } from "@mui/material/styles/createTypography";
import Typography from "@mui/material/Typography";
import React from "react";

// example of a wrapped component with prop interface
interface TitleComponentProps {
    text: string;
    textVarient: Variant;
    className?: string;
} 

export const TextComponent = (props: TitleComponentProps) => {
  return (
    className?: string;
    <Typography className={props.} variant={props.textVarient}>{props.text}</Typography>
  );
};
