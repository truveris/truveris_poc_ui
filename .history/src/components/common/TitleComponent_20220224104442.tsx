import { Container } from "@mui/material";
import React from "react";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <Container maxWidth="xl">
              <Typography variant="h1" component="h1">
        Page Title
      </Typography>
        <TableComponent/>
      </Container>
  );
};
