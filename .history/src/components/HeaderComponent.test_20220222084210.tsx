import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { HeaderComponent } from './HeaderComponent';

test('renders title', () => {
  render(<HeaderComponent />);

  const title = screen.getByText("Truveris POC");
  expect(title).toBeInTheDocument();
});
