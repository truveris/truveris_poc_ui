import { Container } from "@mui/material";
import React from "react";
import { TitleComponent } from "../common/TitleComponent";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <Container maxWidth="xl">
        <TitleComponent text="RFPs Data" textVarient="h4" />
        <TableComponent/>
      </Container>
  );
};
