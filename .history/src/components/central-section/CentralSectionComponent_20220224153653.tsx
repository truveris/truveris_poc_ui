import { Container, Divider } from "@mui/material";
import React from "react";
import { TextComponent } from "../common/TextComponent";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <Container className="central-section-container"  maxWidth="xl">
        <TextComponent text="RFPs Data" textVarient="h5" />
        <Divider variant="middle" />
        <TableComponent/>
      </Container>
  );
};
