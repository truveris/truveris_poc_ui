import { Container } from "@mui/material";
import React from "react";
import { TextComponent } from "../common/TextComponent";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <Container className=""  maxWidth="xl">
        <TextComponent text="RFPs Data" textVarient="h4" />
        <TableComponent/>
      </Container>
  );
};
