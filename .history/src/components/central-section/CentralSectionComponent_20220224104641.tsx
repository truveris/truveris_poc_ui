import { Container } from "@mui/material";
import React from "react";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <Container maxWidth="xl">
        <TitleComponent te />
        <TableComponent/>
      </Container>
  );
};
