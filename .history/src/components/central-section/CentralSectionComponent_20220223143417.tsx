import React from "react";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <div className="central-section-wrapper">
        <TableComponent/>
      </div>
  );
};
