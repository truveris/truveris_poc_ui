import React from "react";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <>
        <h1>RFP</h1>
        <TableComponent/>
      </>
  );
};
