import { Container } from "@mui/material";
import React from "react";
import { TableComponent } from "../table/TableComponent";
import './CentralSectionComponent.css';

export const CentralSectionComponent = () => {
  return (
      <Container maxWidth="lg">
        <h1>RFPs</h1>
        <TableComponent/>
      </Container>
  );
};
