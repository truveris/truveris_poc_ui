import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './app-store/store';
import { App } from './App';

test('renders title', () => {
  render(<App />);

  const title = screen.
  expect(title).toBeInTheDocument();
});
