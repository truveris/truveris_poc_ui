import React, {createContext, useState, useContext} from 'react';

export interface ContextState {
   darkTheme: boolean;
   toggleDarkTheme: () => void;
}

const initialContextState: ContextState = {
   darkTheme: false,
   toggleDarkTheme: () => {
   }
};

export const PocContext = createContext<ContextState>(initialContextState);

export const useReportsContext = () => useContext(PocContext);

// @ts-ignore
export const PocContextProvider = props => {
   const [darkTheme, setDarkTheme] = useState<boolean>(false);

   const toggleDarkTheme = () => {
    setDarkTheme(!darkTheme);
   };

   return (
      <PocContext.Provider
         value={{
            darkTheme,
            toggleDarkTheme,
         }}
      >
         {props.children}
      </PocContext.Provider>
   );
};