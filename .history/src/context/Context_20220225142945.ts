import React, {createContext, useState, useContext} from 'react';

export interface ContextState {
   darkTheme: false;
   toggleDarkTheme: () => void;
}

const initialContextState: ContextState = {
   theme: false,
   setTheme: () => {
   }
};

export const ReportsContext = createContext<ContextState>(initialContextState);

export const useReportsContext = () => useContext(ReportsContext);

// @ts-ignore
export const ReporterContextProvider = props => {
   const [showModal, setShowModalVisibility] = useState<boolean>(false);
   const [modalData, setCurrentModalData] = useState<string[]>([]);

   const toggleModalVisibility = () => {
      setShowModalVisibility(!showModal);
   };

   const setModalData = (data: string[]) => {
      setCurrentModalData(data);
   };

   return (
      <ReportsContext.Provider
         // eslint-disable-next-line
         value={{
            showModal,
            toggleModalVisibility,
            modalData,
            setModalData
         }}
      >
         {props.children}
      </ReportsContext.Provider>
   );
};