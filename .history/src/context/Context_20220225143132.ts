import React, {createContext, useState, useContext} from 'react';

export interface ContextState {
   darkTheme: false;
   toggleDarkTheme: () => void;
}

const initialContextState: ContextState = {
   darkTheme: false,
   toggleDarkTheme: () => {
   }
};

export const ReportsContext = createContext<ContextState>(initialContextState);

export const useReportsContext = () => useContext(ReportsContext);

// @ts-ignore
export const ReporterContextProvider = props => {
   const [darkTheme, setDarkTheme] = useState<boolean>(false);

   const toggleDarkTheme = () => {
    setDarkTheme(!darkTheme);
   };

   return (
      <PocContext.Provider
         // eslint-disable-next-line
         value={{
            darkTheme,
            toggleDarkTheme,
         }}
      >
         {props.children}
      </PocContext.Provider>
   );
};