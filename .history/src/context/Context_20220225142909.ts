import React, {createContext, useState, useContext} from 'react';

export interface ContextState {
   showModal: boolean;
   toggleModalVisibility: () => void;

   modalData: string[];
   setModalData: (data: string[]) => void;
}

const initialContextState: ContextState = {
   theme: false,
   toggleModalVisibility: () => {
   }
};

export const ReportsContext = createContext<ContextState>(initialContextState);

export const useReportsContext = () => useContext(ReportsContext);

// @ts-ignore
export const ReporterContextProvider = props => {
   const [showModal, setShowModalVisibility] = useState<boolean>(false);
   const [modalData, setCurrentModalData] = useState<string[]>([]);

   const toggleModalVisibility = () => {
      setShowModalVisibility(!showModal);
   };

   const setModalData = (data: string[]) => {
      setCurrentModalData(data);
   };

   return (
      <ReportsContext.Provider
         // eslint-disable-next-line
         value={{
            showModal,
            toggleModalVisibility,
            modalData,
            setModalData
         }}
      >
         {props.children}
      </ReportsContext.Provider>
   );
};