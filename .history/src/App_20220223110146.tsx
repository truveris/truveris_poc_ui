import React, { useEffect } from "react";
import "./App.css";
import { HeaderComponent } from "./components/header/HeaderComponent";
import { useAppDispatch } from "./store/hooks";
import { RfpActionTypes } from "./domain/rfp/RfpActionTypes";
import { fetchRfpsActionCreator } from "./domain/rfp/RfpActionCreators";

export const App = () => {
s

  return (
    <div className="app-wrapper">
      <HeaderComponent/>
    </div>
  );
};
