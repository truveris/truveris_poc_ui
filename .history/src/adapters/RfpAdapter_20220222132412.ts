import axios, {AxiosResponse} from 'axios';


export const getReportsAdapter = (url: string): Promise<ReportsDataModel> => {
   const ReportsDataIO = new HalIO(ReportsDataModel);

   return axios.get(url)
      .then((response: AxiosResponse<ReportsDataModel>): ReportsDataModel => {
         return response.data;
      });
}