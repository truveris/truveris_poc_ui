import axios, {AxiosResponse} from 'axios';


export const getReportsAdapter = (url: string): Promise<RfpDataModel> => {
   return axios.get(url)
      .then((response: AxiosResponse<RfpModel>): RfpDataModel => {
         return response.data;
      });
}