import axios, {AxiosResponse} from 'axios';


export const getReportsAdapter = (url: string): Promise<RfpModel> => {
   return axios.get(url)
      .then((response: AxiosResponse<RfpModel>): RfpModel => {
         return response.data;
      });
}