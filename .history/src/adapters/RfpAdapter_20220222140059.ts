import axios, {AxiosResponse} from 'axios';
import { RfpModel } from '../domain/rfp/RfpModel';

export const getReportsAdapter = (url: string): Promise<RfpModel> => {
   return axios.get(url)
      .then((response: AxiosResponse<RfpModel>): RfpModel => {
         return response.data;
      });
}