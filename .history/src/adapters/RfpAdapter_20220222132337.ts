import {AxiosResponse} from 'axios';
import {axiosInstance, deserialize, HalIO} from '@unifocus/js-utils';
import {ReportsDataModel, UpdateExecutionModel} from '../../domain/reports/ReportsModel';

export const getReportsAdapter = (url: string): Promise<ReportsDataModel> => {
   const ReportsDataIO = new HalIO(ReportsDataModel);

   return axiosInstance.get(url)
      .then((response: AxiosResponse<ReportsDataModel>): ReportsDataModel => {
         return response.data;
      });
}