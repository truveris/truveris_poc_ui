import axios, {AxiosResponse} from 'axios';
import { RfpModel } from '../domain/rfp/RfpModel';

export const fetchRfps = (url: string): Promise<RfpModel> => {
   return axios.get(url)
      .then((response: AxiosResponse<RfpModel>): RfpModel => {
         return response.data;
      });
}