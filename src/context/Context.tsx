/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import React, { createContext, useState, useContext } from 'react';

export interface ContextState {
  dark: boolean;
  toggleDarkTheme: () => void;
}

const initialContextState: ContextState = {
  dark: false,
  toggleDarkTheme: () => {
  },
};

export const PocContext = createContext<ContextState>(initialContextState);

export const usePocContext = () => useContext(PocContext);

// @ts-ignore
export const PocContextProvider = (props) => {
  const [dark, setDarkTheme] = useState<boolean>(false);

  const toggleDarkTheme = () => {
    setDarkTheme(!dark);
  };

  return (
    <PocContext.Provider
      value={{
        dark,
        toggleDarkTheme,
      }}
    >
      {props.children}
    </PocContext.Provider>
  );
};
