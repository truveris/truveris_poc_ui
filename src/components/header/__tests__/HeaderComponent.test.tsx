import React from 'react';
import { render, screen } from '@testing-library/react';
import { HeaderComponent } from '../HeaderComponent';

test('renders title', () => {
  render(<HeaderComponent />);

  const title = screen.getByText('Truveris POC');
  expect(title).toBeInTheDocument();
});
