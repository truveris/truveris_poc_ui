import React from 'react';
import './HeaderComponent.css';
import { TextComponent } from '../common/text-component/TextComponent';
import { usePocContext } from '../../context/Context';
import Switch from '@mui/material/Switch';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';

export const HeaderComponent = () => {
  const { toggleDarkTheme } = usePocContext();
  const { dark } = usePocContext();

  const darkModeSwitch: JSX.Element = (
    <div className="modeToggle">
      <LightModeIcon className="modeIcon" />
      {' '}
      <Switch className="dark-mode-switch" onChange={toggleDarkTheme} />
      {' '}
      <DarkModeIcon className="modeIcon" />
    </div>
  );

  return (
    <div className={dark ? 'darkHeader' : 'lightHeader'}>
      <TextComponent text="Truveris POC" textVariant="h5" />

      {darkModeSwitch}
    </div>
  );
};
