import React from 'react';
import { render, screen } from '@testing-library/react';
import { CentralSectionComponent } from '../CentralSectionComponent';
import { renderWithRedux } from '../../../utilities/TestUtils';
import { createStore } from '@reduxjs/toolkit';
import { initialState, rfpReducer } from '../../../domain/rfp/RfpReducer';
import { store } from '../../../store/store';

test('renders text', () => {
  renderWithRedux(
    <CentralSectionComponent />, {store}
  );

  expect(screen.getByTestId('container')).toBeInTheDocument();
});