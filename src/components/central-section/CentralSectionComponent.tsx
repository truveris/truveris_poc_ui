import React from 'react';
import './CentralSectionComponent.css';
import { Container } from '@mui/material';
import { AppStrings } from '../../resources/AppStrings';
import { TableComponent } from '../table/TableComponent';
import { TextComponent } from '../common/text-component/TextComponent';

export const CentralSectionComponent = () => (
  <Container data-testid="container" className="central-section-container" maxWidth="xl">
    <div className="pagetitleWrapper">
      <TextComponent text={AppStrings.viewRfps} textVariant="h4" />
    </div>
    <TableComponent />
  </Container>
);
