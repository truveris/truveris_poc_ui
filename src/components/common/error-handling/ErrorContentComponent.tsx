import React from 'react';
import { AxiosError } from 'axios';
import { DialogContent } from '@mui/material';
import { TextComponent } from '../text-component/TextComponent';
import './ErrorDialogComponent.css';
import { usePocContext } from '../../../context/Context';
import ErrorIcon from '@mui/icons-material/Error';
import { AppStrings } from '../../../resources/AppStrings';

interface ErrorContentComponentProps {
  error: AxiosError | undefined;
}

export const ErrorContentComponent = (props: ErrorContentComponentProps) => {
  const { dark } = usePocContext();

  const error = props.error;

  return (
    // @ts-ignore
    <>
      {error ? (
        <DialogContent>
          <div className="errorSummary">
            <ErrorIcon color="error" fontSize="large" className="errorIcon" />
            <TextComponent
              text={`Error code: ${error.response ? error.response.status : ' '}`}
              textVariant="subtitle1"
            />
          </div>
          <pre className={dark ? 'preCodeDark' : 'preCodeLight'}>
            <code>
              { AppStrings.errorCode}: {error.response ? error.response.status : '' }
              <hr />
              {JSON.stringify(error.response ? error.response.data : '', null, 2)}
              <hr />
              { JSON.stringify(error.response ? error.response.headers : '', null, 2) }
              <hr />
              { error.stack }
            </code>
          </pre>
        </DialogContent>
      ) : (<></>)}
    </>
  );
};
