import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import * as reactRedux from 'react-redux';
import { ErrorDialogComponent } from '../ErrorDialogComponent';
import * as ourActions from '../../../../domain/error-handling/ErrorHandlingActionCreators';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

describe('Test TargetComponent', () => {
  const mockStore = {
    errorHandlingReducer: {
      error: {
        status: 500,
      },
    },
  };

  beforeEach(() => {
    // @ts-ignore
    useDispatchMock.mockImplementation(() => () => {});
    // @ts-ignore
    useSelectorMock.mockImplementation((selector) => selector(mockStore));
  });
  afterEach(() => {
    // @ts-ignore
    useDispatchMock.mockClear();
    // @ts-ignore
    useSelectorMock.mockClear();
  });

  const useSelectorMock = reactRedux.useSelector;
  const useDispatchMock = reactRedux.useDispatch;

  it('error dialog should render title', () => {
    render(<ErrorDialogComponent />);

    expect(screen.getByText('Houston, we have a problem')).toBeInTheDocument();
  });

  // it('should call clearErrorAction', () => {
  //     render(<ErrorDialogComponent/>);
  //     const clearErrorActionSpy = jest.spyOn(ourActions, 'clearErrorActionCreator');
  //     const button = screen.getByText('Logout');
  //     fireEvent.click(button);

  //     expect(clearErrorActionSpy).toHaveBeenCalledTimes(1);
  // });
});
