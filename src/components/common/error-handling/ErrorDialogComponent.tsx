import React, { useEffect, useState } from 'react';
import { AxiosError } from 'axios';
import { errorSelector } from '../../../domain/error-handling/ErrorHandlingSelectors';
import Dialog from '@mui/material/Dialog';
import { Button, DialogActions, DialogTitle } from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../../hooks/hooks';
import './ErrorDialogComponent.css';
import { clearErrorActionCreator } from '../../../domain/error-handling/ErrorHandlingActionCreators';
import { ErrorContentComponent } from './ErrorContentComponent';
import { AppStrings } from '../../../resources/AppStrings';

export const ErrorDialogComponent = () => {
  const dispatch = useAppDispatch();
  const errorLog: AxiosError | undefined = useAppSelector(errorSelector);
  const [error, setError] = useState<AxiosError | undefined>(undefined);

  useEffect(() => {
    if (errorLog) {
      setError(errorLog);
    } else {
      setError(undefined);
    }
  }, [errorLog]);

  return (
    <Dialog
      open={!!error}
      keepMounted={true}
    >
      <DialogTitle>{AppStrings.errorDialogTitle}</DialogTitle>
      <ErrorContentComponent error={error} />
      <DialogActions>
        <Button onClick={() => dispatch(clearErrorActionCreator(undefined))}>{AppStrings.close}</Button>
      </DialogActions>
    </Dialog>
  );
};
