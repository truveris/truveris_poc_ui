import React from 'react';
import { render, screen } from '@testing-library/react';
import { TextComponent, TitleComponentProps } from '../TextComponent';
import { Variant } from '@mui/material/styles/createTypography';

const props: TitleComponentProps = {
  text: 'test test',
  textVariant: 'h1' as Variant,
};

test('renders text', () => {
  render(<TextComponent {...props} />);

  const textComponent = screen.getByRole('textComponent');
  expect(textComponent).toBeInTheDocument();
  expect(textComponent).toHaveTextContent('test test');
});
