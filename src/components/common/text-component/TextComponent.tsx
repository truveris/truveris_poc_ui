import { Variant } from '@mui/material/styles/createTypography';
import Typography from '@mui/material/Typography';
import React from 'react';

// example of a wrapped component with prop interface
export interface TitleComponentProps {
  text: string;
  textVariant: Variant;
  className?: string;
}

export const TextComponent = (props: TitleComponentProps) => (
  <Typography role="textComponent" className={props.className} variant={props.textVariant}>
    {props.text}
  </Typography>
);
