import React, { useState } from 'react';
import { RfpModel } from '../../domain/rfp/RfpModels';
import { rfpsReceivedSelector, rfpsSelector } from '../../domain/rfp/RfpSelectors';
import { useAppSelector } from '../../hooks/hooks';
import { DataGrid, GridColumns } from '@mui/x-data-grid';
import './TableComponent.css';
import { rfpColumnDef } from './TableHelpers';
import { usePocContext } from '../../context/Context';

export const TableComponent = () => {
  const [rfpsColumns] = useState<GridColumns>(rfpColumnDef);
  const rfpsRows: RfpModel[] = useAppSelector(rfpsSelector);
  const rfpsReceived: boolean = useAppSelector(rfpsReceivedSelector);
  const { dark } = usePocContext();

  return (
    <DataGrid
      className={dark ? 'darkTable' : 'lightTable'}
      rows={rfpsRows}
      columns={rfpsColumns}
      pageSize={10}
      autoHeight={true}
      rowsPerPageOptions={[10]}
      loading={!rfpsReceived}
      rowHeight={75}

    />
  );
};
