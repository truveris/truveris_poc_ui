import { GridColumns, GridValueFormatterParams } from '@mui/x-data-grid';
import { AppStrings } from '../../resources/AppStrings';
import { formatDate } from '../../utilities/DateFormatter';

const dateValueFormatter = (params: GridValueFormatterParams) => formatDate(params.value as string);

export const rfpColumnDef: GridColumns = [
  { field: 'id', headerName: AppStrings.id, maxWidth: 60 },
  { field: 'client_name', headerName: AppStrings.clientName, flex: 1 },
  { field: 'contract_pbm', headerName: AppStrings.contractPbm, flex: 1 },
  { field: 'rfp_status', headerName: AppStrings.rfpStatus, flex: 1 },
  {
    field: 'awarded_at',
    headerName: AppStrings.awardedAt,
    flex: 1,
    type: 'date',
    valueFormatter: dateValueFormatter,
  },
  {
    field: 'implementation_date',
    headerName: AppStrings.implementationDate,
    flex: 1,
    type: 'date',
    valueFormatter: dateValueFormatter,
  },
  {
    field: 'next_milestone',
    headerName: AppStrings.nextMilestone,
    flex: 1,
    type: 'date',
    valueFormatter: dateValueFormatter,
  },
  {
    field: 'rfp_sent',
    headerName: AppStrings.rfpSent,
    flex: 1,
    type: 'date',
    valueFormatter: dateValueFormatter,
  },
  { field: 'year', headerName: AppStrings.year, type: 'number', maxWidth: 60 },
  { field: 'notes', headerName: AppStrings.notes, flex: 1 },
];
