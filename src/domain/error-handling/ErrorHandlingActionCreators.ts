import { createAction } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import { ErrorHandlingActionTypes } from './ErrorHandlingActionTypes';

export const storeErrorActionCreator = createAction<AxiosError>(ErrorHandlingActionTypes.STORE_ERROR);
export const clearErrorActionCreator = createAction<undefined>(ErrorHandlingActionTypes.CLEAR_ERROR);
