import { RootState } from '../../store/store';

const local = (state: RootState) => state.errorHandlingReducer;

export const errorSelector = (state: RootState) => local(state).error;
