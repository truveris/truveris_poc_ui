import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import { clearErrorActionCreator, storeErrorActionCreator } from './ErrorHandlingActionCreators';

export interface ErrorHandlingState {
  error: AxiosError | undefined;
}

export const initialState: ErrorHandlingState = {
  error: undefined,
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handleStoreError = (state: ErrorHandlingState, action: PayloadAction<AxiosError>) => ({
  error: action.payload,
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handleClear = (state: ErrorHandlingState, action: PayloadAction<undefined>) => ({
  error: undefined,
});

export const errorHandlingReducer = createReducer(initialState, (builder) => {
  // @ts-ignore
  builder
    .addCase(storeErrorActionCreator, handleStoreError)
    .addCase(clearErrorActionCreator, handleClear);
});
