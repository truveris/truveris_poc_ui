export enum ErrorHandlingActionTypes {
  STORE_ERROR = 'STORE_ERROR',
  CLEAR_ERROR = 'CLEAR_ERROR',
}
