import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { storeRfpsActionCreator, updateRfpsReceivedActionCreator } from './RfpActionCreators';
import { RfpModel } from './RfpModels';

export interface RfpReducerState {
  rfps: RfpModel[];
  rfpsReceived: boolean;
}

export const initialState: RfpReducerState = {
  rfps: [],
  rfpsReceived: false,
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handleStoreRfps = (state: RfpReducerState, action: PayloadAction<RfpModel[]>) => ({
  rfps: action.payload,
  rfpsReceived: true,
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handleUpdateRfpsReceived = (state: RfpReducerState, action: PayloadAction<boolean>) => ({
  rfps: state.rfps,
  rfpsReceived: action.payload,
});

export const rfpReducer = createReducer(initialState, (builder) => {
  // @ts-ignore
  builder
    .addCase(storeRfpsActionCreator, handleStoreRfps)
    .addCase(updateRfpsReceivedActionCreator, handleUpdateRfpsReceived);
});

// ****createSlice example****
// export const rfpsSlice = createSlice({
//   name: 'rfpSlice',
//   initialState,
//   reducers: {
//     storeRfpsAction: (state, action) => {
//         return {
//            rfps: action.payload
//         };
//       }
//   }
// });

// export const { storeRfpsAction } = rfpsSlice.actions;

// export default rfpsSlice.reducer;
