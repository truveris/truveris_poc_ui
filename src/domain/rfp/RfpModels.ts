import { serializable } from 'serializr';

export class RfpModel {
  @serializable
    id: number;

  @serializable
    awarded_at: string;

  @serializable
    client_name: string;

  @serializable
    contract_pbm: string;

  @serializable
    implementation_date: string;

  @serializable
    next_milestone: string;

  @serializable
    rfp_sent: string;

  @serializable
    notes: string;

  @serializable
    rfp_status: string;

  @serializable
    year: string;
}
