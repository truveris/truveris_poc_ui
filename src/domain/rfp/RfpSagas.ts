/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { SagaIterator } from 'redux-saga';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { fetchRfpsAdapter } from '../../adapters/RfpAdapter';
import { storeRfpsActionCreator, updateRfpsReceivedActionCreator } from './RfpActionCreators';
import { RfpActionTypes } from './RfpActionTypes';
import { RfpModel } from './RfpModels';

export function* getRfpsSaga(): SagaIterator {
  const rfpLimit: number = 15;
  const rfpsUrl: string = `up/api/rfp?limit=${rfpLimit}`;
  yield put(updateRfpsReceivedActionCreator(false));

  const rfps: RfpModel[] = yield call(fetchRfpsAdapter, rfpsUrl);
  if (rfps) {
    yield put(storeRfpsActionCreator(rfps as RfpModel[]));
  }
}

export function* watchRfpsAsyncActions(): SagaIterator {
  yield all([
    takeLatest(RfpActionTypes.FETCH_RFPS, getRfpsSaga),
  ]);
}
