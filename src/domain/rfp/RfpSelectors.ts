import { RootState } from '../../store/store';

export const local = (state: RootState) => state.rfpReducer;

export const rfpsSelector = (state: RootState) => local(state).rfps;
export const rfpsReceivedSelector = (state: RootState) => local(state).rfpsReceived;
