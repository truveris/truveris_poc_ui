import axios, { AxiosResponse } from 'axios';
import { RfpModel } from '../../domain/rfp/RfpModels';
import { fetchRfpsAdapter } from '../RfpAdapter';

jest.mock('axios');

describe('fetchRfpsAdapter', () => {
  test('should return rfp list', async () => {
    const rfps: RfpModel[] = [
      {
        awarded_at: '2022-01-27',
        client_name: 'Canonical Inc.',
        contract_pbm: 'ExpressX International',
        id: 13,
        implementation_date: '2023-01-01',
        next_milestone: '2022-01-02',
        notes: 'Awarded RFP',
        rfp_sent: '2022-01-27',
        rfp_status: 'Awarded - ScripJimmyCo Inc.',
        year: '2022',
      },
    ];

    const mockedResponse: AxiosResponse = {
      data: rfps,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    // @ts-ignore
    axios.get.mockResolvedValue(mockedResponse);

    expect(axios.get).not.toHaveBeenCalled();
    const data = await fetchRfpsAdapter('url');
    expect(axios.get).toHaveBeenCalled();
    expect(data).toEqual(rfps);
  });
});
