/* eslint-disable arrow-body-style */
import axios, { AxiosError, AxiosResponse } from 'axios';
import { RfpModel } from '../domain/rfp/RfpModels';
import { httpErrorHandler } from '../utilities/ErrorHandler';

export const fetchRfpsAdapter = (url: string): Promise<RfpModel[] | void> => {
  // const jwt: string | null = localStorage.getItem('jwt');
  // const headers: AxiosRequestHeaders = {
  //   authorization: `Bearer ${jwt ? jwt : ''}`,
  // };

  return axios
    .get(url, { withCredentials: true })
    .then((response: AxiosResponse<RfpModel[]>): RfpModel[] => response.data)
    .catch((error: AxiosError) => httpErrorHandler(error));
};
