export const saveToLocalStorage = (name: string, value: string | null) => {
  if (value) {
    localStorage.setItem(name, value);
  }
};
