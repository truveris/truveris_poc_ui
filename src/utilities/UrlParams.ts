export const getParamFromUrl = (queryString: string, param: string): string | null => {
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(param);
};
