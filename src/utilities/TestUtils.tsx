/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { ReactNode } from 'react';
import { AnyAction, Action, createStore, Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { render, RenderResult } from '@testing-library/react';
import { rfpReducer } from '../domain/rfp/RfpReducer';

interface RenderWithRedux<
  S = any,
  A extends Action = AnyAction,
  I extends S = any,
> {
  (
    ui: ReactNode,
    reduxOptions: {
      store?: Store<S, A>
      initialState?: I
    }
  ): RenderResult & {
    store: Store<S, A>
  }
}

export const renderWithRedux: RenderWithRedux = (
  ui,
  { initialState, store = createStore(rfpReducer, initialState) } = {},
) => ({
  ...render(<Provider store={store}>{ui}</Provider>),
  store,
});
