import React from 'react';
import 'jest';
import { getParamFromUrl } from '../UrlParams';
import { httpErrorHandler } from '../ErrorHandler';
import { AxiosError } from 'axios';

let windowOverride: Window | null;

export const overrideWindow = (window: Window | null): void => {
  windowOverride = window;
};

describe('httpErrorHandler', () => {
  it('httpErrorHandler should redirect to login if auth fails', () => {
    window.open = jest.fn();

    const error = {
      response: {
        status: 403,
      },
    };
    httpErrorHandler(error as AxiosError);

    expect(window.open).toHaveBeenCalled();
    expect(window.open).toHaveBeenCalledWith('http://localhost/login/clear', '_self');
  });

  it('httpErrorHandler should not redirect if there is some other error', () => {
    window.open = jest.fn();

    const error = {
      response: {
        status: 500,
      },
    };
    httpErrorHandler(error as AxiosError);

    expect(window.open).not.toHaveBeenCalled();
  });
});
