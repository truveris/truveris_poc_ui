/* eslint-disable no-console */
import { AxiosError } from 'axios';
import { storeErrorActionCreator } from '../domain/error-handling/ErrorHandlingActionCreators';
import { store } from '../store/store';

export const httpErrorHandler = (error: AxiosError) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);

    if (error.response.status === 403 || error.response.status === 400) {
      window.open(`${window.location.origin}/login/clear`, '_self');
    } else {
      store.dispatch(storeErrorActionCreator(error));
    }
  } else if (error.request) {
    // The request was made but no response was received
    console.log(error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error', error.message);
  }
  console.log(error.config);
};
