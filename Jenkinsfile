
pipeline {
  agent {
    label 'instance'
  }

  options {
    skipDefaultCheckout()
  }

  environment {
    IMAGE = 'core-payer-truveris-poc-ui'
    ECR = '074919163324.dkr.ecr.us-east-1.amazonaws.com'
    AWS_REGION = 'us-east-1'
  }

  stages {
    stage("Checkout source") {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: scm.branches,
          doGenerateSubmoduleConfigurations: scm.doGenerateSubmoduleConfigurations,
          extensions: scm.extensions + [[$class: 'CloneOption', noTags: false, reference: '', shallow: false, depth: 0]],
                    userRemoteConfigs: scm.userRemoteConfigs
        ])
          script {
            env.VERSION=sh(script: "git describe --tags --always | sed 's/-/./g'", returnStdout: true).trim()
            env.CURRENT_BRANCH=sh(script: "git rev-parse --abbrev-ref HEAD", returnStdout: true).trim()
            env.CONTAINER_ID=sh(script: "git rev-parse --short ${env.CURRENT_BRANCH}", returnStdout: true).trim()
            env.ECR_SERVER='074919163324.dkr.ecr.us-east-1.amazonaws.com/core-payer-truveris-poc-ui'
          }
      }
    }

    stage("Test") {
      steps {
        echo "Running tests"
        sh "make tests"
      }
    }

    stage("Build") {
      steps {
        echo "Building"
        sh "make build"
      }
    }

    stage("Upload") {
      steps {
        sh "make push"
      }
    }

    // stage("Deploy") {
    //   when {
    //     branch 'master'
    //   }

    //   steps {
    //     build job: 'payer-truveris_poc_ui-deploy',
    //       parameters: [
    //         gitParameter(name: 'BRANCH', value: 'origin/master'),
    //         string(name: 'TICKET', value: 'default'),
    //       ],
    //       quietPeriod: 2,
    //       wait: false
    //   }
    // }
  }

  post {
      success {
          script {
              currentBuild.description = "Version: ${env.VERSION}<br />Container ID: ${CONTAINER_ID}"
          }
      }
      always {
          script {
              currentBuild.result = currentBuild.result ?: 'SUCCESS'
              if (currentBuild.result == 'SUCCESS') {
                  env.RESULT = 'SUCCESSFUL'
              }
              else {
                  env.RESULT = 'FAILED'
              }
          }
          bitbucketStatusNotify(buildState: "${env.RESULT}")
      }
  }
}
